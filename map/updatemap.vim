function! VLOldMapToVLL()
    silent! %s/\V.|/X 0/g
    silent! %s/\V_|/  0/g
    silent! %s/\V$|/$ 0/g
    silent! %s/\V#|/$ 1/g
    silent! %s/\V%|/$ 1/g
    silent! %s/\V#|/# 1/g
    silent! %s/\V%|/# 2/g
    silent! %s/\V_l/ L1/g
    silent! %s/\V_h/ H1/g
    silent! %s/\V_v/ V1/g
    silent! %s/\V_f/ F1/g
    silent! %s/\V_z/ Z1/g
    silent! %s/\V_Z/ Z2/g
    silent! %s/\V_F/ F2/g
    silent! %s/\V_V/ V2/g
    silent! %s/\V_H/ H2/g
    silent! %s/\V_L/ L2/g
endfunction
