#ifndef VIEWS_ACTION_H
#define VIEWS_ACTION_H

#include <SDL2/SDL.h>

#include "../models/action.h"

/* These functions all require the rendering area as well as the width and
 * height of the board so as to accurately render the results. */

/* Requires width and height of game to determine tile size. */
void render_action(struct Action act, SDL_Rect area, int w, int h);

/* Renders the 'unspawn action' */
void render_unspawn_action(struct Action unspawn, SDL_Rect area, int w, int h);

/* Renders the action as a choice. */
void render_action_option(struct Action act, SDL_Rect area, int w, int h);

/* Renders the spawn selection options. */
void render_spawn_selection(SDL_Rect area);

/* Returns the result of a click at the given coordinate in the given area for a
 * spawn selection. */
enum UnitType get_spawn_selection(int x, int y, SDL_Rect area);

/* Returns the x coordinate relative to the area for a game of given w and h */
int get_tile_x(int x, SDL_Rect area, int w);

/* Returns the y coordinate relative to the area for a game of given w and h */
int get_tile_y(int y, SDL_Rect area, int h);
#endif /* VIEWS_ACTION_H */
