#ifndef VIEWS_GAME_H
#define VIEWS_GAME_H

#include "../models/game.h"

#include <SDL2/SDL.h>

void gameview_draw(struct Game game, int team, SDL_Rect area);

#endif /* VIEWS_GAME_H */
