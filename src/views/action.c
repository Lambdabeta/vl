#include "action.h"

#include "../res/sprites.h"
#include "../res/globals.h"
#include "../tools/minmax.h"
#include "../tools/within.h"

double action_theta(struct Action act)
{
    if (act.source.x < act.target.x) {
        if (act.source.y < act.target.y) 
            return -135.0; /* NORTH-WEST */
        else if (act.source.y > act.target.y)
            return 135.0; /* SOUTH-WEST */
        else
            return 180.0; /* WEST */
    } else if (act.source.x > act.target.x) {
        if (act.source.y < act.target.y)
            return -45.0; /* NORTH-EAST */
        else if (act.source.y > act.target.y)
            return 45.0; /* SOUTH-EAST */
        else
            return 0.0; /* EAST */
    } else {
        if (act.source.y < act.target.y)
            return -90.0; /* NORTH */
        else if (act.source.y > act.target.y)
            return 90.0; /* SOUTH */
        else
            return 0.0; /* SPAWN, ERROR! */
    }
}

SDL_Rect action_rect(struct Action move, SDL_Rect area, int w, int h)
{
    SDL_Rect ret;

    int source_left = area.x + move.source.x * (area.w / w);
    int source_top = area.y + move.source.y * (area.h / h);
    int source_right = area.x + (move.source.x + 1) * (area.w / w);
    int source_bottom = area.y + (move.source.y + 1) * (area.h / h);

    int target_left = area.x + move.target.x * (area.w / w);
    int target_top = area.y + move.target.y * (area.h / h);
    int target_right = area.x + (move.target.x + 1) * (area.w / w);
    int target_bottom = area.y + (move.target.y + 1) * (area.h / h);

    ret.x = minint(source_left,target_left);
    ret.y = minint(source_top,target_top);
    ret.w = maxint(source_right,target_right) - ret.x;
    ret.h = maxint(source_bottom,target_bottom) - ret.y;

    return ret;
}

static void render_action_move(struct Action move, SDL_Rect area, int w, int h)
{
    double theta = action_theta(move) + 180;
    SDL_Rect rect = action_rect(move, area, w, h);

    SDL_RenderCopyEx(renderer,sprites[SPR_ARROW_MOVE],NULL,&rect,theta,NULL,SDL_FLIP_NONE);
}

static void render_action_act(struct Action act, SDL_Rect area, int w, int h)
{
    double theta = action_theta(act) + 180;
    SDL_Rect rect = action_rect(act, area, w, h);

    SDL_RenderCopyEx(renderer,sprites[SPR_ARROW_ACT],NULL,&rect,theta,NULL,SDL_FLIP_NONE);
}

static void render_action_spawn(struct Action act, SDL_Rect area, int w, int h)
{
    enum SpriteID sid;
    SDL_Rect rect;
    switch (act.unit) {
        case UNIT_TYPE_VAMPIRE: sid = SPR_SPAWNED_VAMPIRE; break;
        case UNIT_TYPE_LEPRECHAUN: sid = SPR_SPAWNED_LEPRECHAUN; break;
        case UNIT_TYPE_HUMAN: sid = SPR_SPAWNED_HUMAN; break;
        case UNIT_TYPE_FAIRY: sid = SPR_SPAWNED_FAIRY; break;
        case UNIT_TYPE_ZOMBIE: sid = SPR_SPAWNED_ZOMBIE; break;
        default: return;
    }

    rect.x = area.x + act.source.x * (area.w / w);
    rect.y = area.y + act.source.y * (area.y / h);
    rect.w = area.w / w;
    rect.h = area.h / h;

    SDL_RenderCopy(renderer,sprites[sid],NULL,&rect);
}

void render_action(struct Action act, SDL_Rect area, int w, int h)
{
    if (act.is_special) {
        render_action_act(act,area,w,h);
    } else if (!action_is_spawn(act)) {
        render_action_move(act,area,w,h);
    } else {
        render_action_spawn(act,area,w,h);
    }      
}

void render_unspawn_action(struct Action act, SDL_Rect area, int w, int h)
{
    SDL_Rect rect;
    rect.x = area.x + act.source.x * (area.w / w);
    rect.y = area.y + act.source.y * (area.y / h);
    rect.w = area.w / w;
    rect.h = area.h / h;

    SDL_RenderCopy(renderer,sprites[SPR_UNSPAWN_OPTION],NULL,&rect);
}

void render_action_option(struct Action act, SDL_Rect area, int w, int h)
{
    SDL_Rect rect;
    rect.x = area.x + act.target.x * (area.w / w);
    rect.y = area.y + act.target.y * (area.h / h);
    rect.w = area.w / w;
    rect.h = area.h / h;

    SDL_RenderCopy(renderer,
            sprites[act.is_special ? SPR_SELECT_ACT : SPR_SELECT_MOVE], 
            NULL, &rect);
}

void render_spawn_selection(SDL_Rect area)
{
    SDL_Rect rect;
    rect.x = area.x; rect.y = area.y; rect.w = area.w / 3; rect.h = area.h / 2;

    SDL_RenderCopy(renderer,sprites[SPR_SPAWN_VAMPIRE],NULL,&rect);
    rect.x += rect.w; 
    SDL_RenderCopy(renderer,sprites[SPR_SPAWN_LEPRECHAUN],NULL,&rect);
    rect.x += rect.w;
    SDL_RenderCopy(renderer,sprites[SPR_SPAWN_HUMAN],NULL,&rect);
    rect.x = area.x; rect.y += rect.h;
    SDL_RenderCopy(renderer,sprites[SPR_SPAWN_FAIRY],NULL,&rect);
    rect.x += rect.w;
    SDL_RenderCopy(renderer,sprites[SPR_SPAWN_ZOMBIE],NULL,&rect);
    rect.x += rect.w;
    SDL_RenderCopy(renderer,sprites[SPR_SPAWN_NONE],NULL,&rect);
}

enum UnitType get_spawn_selection(int x, int y, SDL_Rect area)
{
    SDL_Rect rect;
    rect.x = area.x; rect.y = area.y; rect.w = area.w / 3; rect.h = area.h / 2;

    if (within(x,y,rect)) return UNIT_TYPE_VAMPIRE;
    rect.x += rect.w;
    if (within(x,y,rect)) return UNIT_TYPE_LEPRECHAUN;
    rect.x += rect.w;
    if (within(x,y,rect)) return UNIT_TYPE_HUMAN;
    rect.x = area.x; rect.y += rect.h;
    if (within(x,y,rect)) return UNIT_TYPE_FAIRY;
    rect.x += rect.w;
    if (within(x,y,rect)) return UNIT_TYPE_ZOMBIE;
    rect.x += rect.w;
    if (within(x,y,rect)) return UNIT_TYPE_NONE;

    return UNIT_TYPE_UNKNOWN;
}

int get_tile_x(int x, SDL_Rect area, int w)
{
    /*
    int tile_width = area.w / w;
    int relx = x - area.x;

    return relx / tile_width;
    */
    return w * (x - area.x) / area.w;
}

int get_tile_y(int y, SDL_Rect area, int h)
{
    return h * (y - area.y) / area.h;
}


