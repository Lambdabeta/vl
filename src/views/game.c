#include "game.h"

#include "../res/sprites.h"
#include "../res/globals.h"

static void render_tile(struct Tile t, int team, SDL_Rect area)
{
    enum SpriteID tile;
    enum SpriteID unit;

    switch (t.type) {
        case TILE_TYPE_BASE: tile = SPR_TILE_BASE; break;
        case TILE_TYPE_PIT: tile = SPR_TILE_PIT; break;
        case TILE_TYPE_FLOOR: tile = SPR_TILE_FLOOR; break;
        case TILE_TYPE_PRODUCER: tile = SPR_TILE_PRODUCER; break;
        case TILE_TYPE_UNKNOWN: tile = SPR_TILE_UNKNOWN; break;
        default: tile = MAX_SPRITE_ID; 
    }

    if (t.team == team || team == 0) {
        switch (t.unit) {
            case UNIT_TYPE_VAMPIRE: unit = SPR_UNIT_VAMPIRE; break;
            case UNIT_TYPE_LEPRECHAUN: unit = SPR_UNIT_LEPRECHAUN; break;
            case UNIT_TYPE_HUMAN: unit = SPR_UNIT_HUMAN; break;
            case UNIT_TYPE_FAIRY: unit = SPR_UNIT_FAIRY; break;
            case UNIT_TYPE_ZOMBIE: unit = SPR_UNIT_ZOMBIE; break;
            default: unit = MAX_SPRITE_ID; 
        }
    } else {
        switch (t.unit) {
            case UNIT_TYPE_VAMPIRE: unit = SPR_ENEMY_UNIT_VAMPIRE; break;
            case UNIT_TYPE_LEPRECHAUN: unit = SPR_ENEMY_UNIT_LEPRECHAUN; break;
            case UNIT_TYPE_HUMAN: unit = SPR_ENEMY_UNIT_HUMAN; break;
            case UNIT_TYPE_FAIRY: unit = SPR_ENEMY_UNIT_FAIRY; break;
            case UNIT_TYPE_ZOMBIE: unit = SPR_ENEMY_UNIT_ZOMBIE; break;
            default: unit = MAX_SPRITE_ID; 
        }
    }

    if (tile != MAX_SPRITE_ID) 
        SDL_RenderCopy(renderer,sprites[tile],NULL,&area);

    if (unit != MAX_SPRITE_ID)
        SDL_RenderCopy(renderer,sprites[unit],NULL,&area);
}

void gameview_draw(struct Game game, int team, SDL_Rect area)
{
    int x,y;

    for (y = 0; y < game.h; ++y) {
        for (x = 0; x < game.w; ++x) {
            SDL_Rect r;
            r.x = area.x + x * (area.w / game.w);
            r.y = area.y + y * (area.h / game.h);
            r.w = area.w / game.w;
            r.h = area.h / game.h;
            render_tile(game.tiles[y][x],team,r);
        }
    }
}
