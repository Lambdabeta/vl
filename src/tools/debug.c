#include "debug.h"

#include <SDL2/SDL.h>

void error_popup(const char *title, const char *msg)
{
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,title,msg,NULL))
        printf("%s: %s\n",title,msg);
}
