#include "boilerplate.h"

#include "debug.h"

#include "../res/strings.h"
#include "../res/ints.h"
#include "../res/globals.h"
#include "../res/sprites.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#if defined(_WIN32)
    #include <winsock2.h>
#endif

static int init_sdl(void) 
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        ERROR(STR_SDL_INIT_ERROR);
        return 1;
    }
    return 0;
}

static int init_img(void)
{
    if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
        ERROR(STR_IMG_INIT_ERROR);
        return 1;
    }
    return 0;
}

static int init_ttf(void) 
{
    if (TTF_Init() < 0) {
        ERROR(STR_TTF_INIT_ERROR);
        return 1;
    }
    return 0;
}

static int init_mix(void)
{
    if (Mix_Init(MIX_INIT_MP3) != MIX_INIT_MP3 || 
        Mix_OpenAudio(MIX_DEFAULT_FREQUENCY,
                      MIX_DEFAULT_FORMAT,
                      MIX_DEFAULT_CHANNELS,4096) < 0) {
        ERROR(STR_MIX_INIT_ERROR);
        return 1;
    }
    return 0;
}

static int create_window(void)
{
    if (!(window = SDL_CreateWindow(STR_WINDOW_TITLE,
                           SDL_WINDOWPOS_UNDEFINED,
                           SDL_WINDOWPOS_UNDEFINED,
                           INT_WINDOW_WIDTH,
                           INT_WINDOW_HEIGHT,
                           SDL_WINDOW_SHOWN))) {
        ERROR(STR_WIN_INIT_ERROR);
        return 1;
    }
    return 0;
}

static int create_renderer(void) 
{
    if (!(renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED))) {
        ERROR(STR_REN_INIT_ERROR);
        return 1;
    }
    return 0;
}

static int init_sockets(void)
{
#if defined(_WIN32)
    WSADATA wsaData;
    if (!WSAStartup(MAKEWORD(2,2),&wsaData))
        return 1;
#endif
    return 0;
}

int boil_init(void)
{
    return 
        init_sdl() ||
        init_img() ||
        init_ttf() ||
        init_mix() ||
        init_sockets();
}

static int texturize_sprites(void)
{
    int i;
    for (i = 0; i < MAX_SPRITE_ID; ++i) {
        sprites[i] = SDL_CreateTextureFromSurface(renderer,(SDL_Surface*)sprites[i]);
        if (!sprites[i]) {
            ERROR("CANT TEXTURIZE!");
            return 1;
        }
    }
    return 0;
}

int boil_init_window(void)
{
    return create_window() || 
           create_renderer() ||
           texturize_sprites();
}

void boil_close(void)
{
#if defined(_WIN32)
    WSACleanup();
#endif
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    Mix_CloseAudio();
    Mix_Quit();
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}
