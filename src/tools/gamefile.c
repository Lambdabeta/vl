#include "gamefile.h"

#include <stdio.h>
#include <stdlib.h>

struct Game gamefile_load(const char *fname)
{
    struct Game ret;
    int x,y;
    FILE *file = fopen(fname,"r");

    fscanf(file,"%d %d\n",&ret.w,&ret.h);

    ret.tiles = malloc(sizeof(struct Tile*) * ret.h);
    for (y = 0; y < ret.h; ++y) {
        ret.tiles[y] = malloc(sizeof(struct Tile) * ret.w);
        
        for (x = 0; x < ret.w; ++x) {
            char type;
            char unit;
            char team;
            fscanf(file,"%c%c%c",&type,&unit,&team);
            ret.tiles[y][x].type = type;
            ret.tiles[y][x].unit = unit;
            ret.tiles[y][x].team = team - '0';
        }
        fgetc(file);
    }

    fclose(file);
    return ret;
}

