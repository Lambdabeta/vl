#ifndef TOOLS_WITHIN_H
#define TOOLS_WITHIN_H

#include <SDL2/SDL.h>

int within(int x, int y, SDL_Rect check);

#endif /* TOOLS_WITHIN_H */
