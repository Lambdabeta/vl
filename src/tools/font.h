#ifndef TOOLS_FONT_H
#define TOOLS_FONT_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

SDL_Texture *render_text(TTF_Font *font, const char *txt, SDL_Color col, int *w, int *h);

#endif /* TOOLS_FONT_H */
