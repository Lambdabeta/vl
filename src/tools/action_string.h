#ifndef TOOLS_ACTION_STRING_H
#define TOOLS_ACTION_STRING_H

#include "../models/action.h"
#include <stdio.h>

/* Returns the size in characters of a string which will hold the representation
 * of an action. */
int action_string_size(void);

/* Fills the given buffer with the string representation of the action. */
void action_string(const struct Action action, char *outstr);

/* Prints the given action to the given stream using fprintf. */
void action_print(const struct Action action, FILE *stream);

/* Verbose versions of the above: */
int action_string_size_verbose(void);
void action_string_verbose(const struct Action action, char *outstr);
void action_print_verbose(const struct Action action, FILE *stream);

/* TODO: also load from strings/streams */
#endif /* TOOLS_GAME_STRING_H */
