#include "button.h"
#include "within.h"
#include "font.h"
#include "centered.h"

#include "../res/fonts.h"
#include "../res/colours.h"
#include "../res/sprites.h"
#include "../res/globals.h"

static enum SpriteID sids[] = { SPR_BUTTON, SPR_BUTTON_HOVER, SPR_BUTTON_PRESSED };

struct Button button_new(const char *txt, SDL_Rect rect)
{
    struct Button ret = { 0 };
    ret.text = render_text(font,txt,COL_BUTTON_TEXT,&ret.w,&ret.h);
    ret.rect = rect;
    ret.state = 0;

    return ret;
}

int button_process(struct Button *b, const SDL_Event *e)
{
    switch (e->type) {
        case SDL_MOUSEMOTION:
            if (!within(e->motion.x,e->motion.y,b->rect)) {
                b->state = 0;
                return 0;
            }
            
            if (e->motion.state & SDL_BUTTON_LMASK) b->state = 2;
            else b->state = 1;
            return 0;
        case SDL_MOUSEBUTTONDOWN:
            if (b->state == 1 && e->button.button == SDL_BUTTON_LEFT)
                b->state = 2;
            return 0;
        case SDL_MOUSEBUTTONUP:
            return b->state == 2;
        default:
            return 0;
    }
}

void button_draw(struct Button b)
{
    SDL_RenderCopy(renderer,sprites[sids[b.state]],NULL,&b.rect);

    render_centered(b.text,b.w,b.h,b.rect);
}

void button_free(struct Button b)
{
    SDL_DestroyTexture(b.text);
}
