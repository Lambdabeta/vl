#include "game_string.h"

int game_string_size(const struct Game game)
{
    return game.w * game.h * 3 + game.h + 1;
}

void game_string(const struct Game game, char *outstr)
{
    int x,y;

    for (y = 0; y < game.h; ++y) {
        for (x = 0; x < game.w; ++x) {
            *outstr = game.tiles[y][x].team + '0'; outstr++;
            *outstr = game.tiles[y][x].type; outstr++;
            *outstr = game.tiles[y][x].unit; outstr++;
        }
        *outstr = '\n'; outstr++;
    }
    *outstr = 0;
}

void game_print(const struct Game game, FILE *stream)
{
    int x,y;

    for (y = 0; y < game.h; ++y) {
        for (x = 0; x < game.w; ++x) {
            fprintf(stream,"%d%c%c",
                    game.tiles[y][x].team,
                    game.tiles[y][x].type,
                    game.tiles[y][x].unit);
        }
        fprintf(stream,"\n");
    }
}
