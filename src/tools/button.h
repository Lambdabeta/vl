#ifndef TOOLS_BUTTON_H
#define TOOLS_BUTTON_H

#include <SDL2/SDL.h>

struct Button {
    SDL_Texture *text;
    SDL_Rect rect;
    int w,h;
    int state; /* 0 = nothing, 1 = hover, 2 = pressed */
};

struct Button button_new(const char *txt, SDL_Rect rect);
int button_process(struct Button *b, const SDL_Event *e); /* returns 1 if the button is clicked. */
void button_draw(struct Button b);
void button_free(struct Button b);

#endif /* TOOLS_BUTTON_H */
