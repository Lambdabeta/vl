#ifndef TOOLS_CENTERED_H
#define TOOLS_CENTERED_H

#include <SDL2/SDL.h>

void render_centered(SDL_Texture *tex, int w, int h, SDL_Rect dst);

#endif /* TOOLS_CENTERED_H */
