#ifndef TOOLS_MINMAX_H
#define TOOLS_MINMAX_H

int minint(int a, int b);
int maxint(int a, int b);

#endif /* TOOLS_MINMAX_H */
