#include "centered.h"

#include "../res/globals.h"

void render_centered(SDL_Texture *tex, int w, int h, SDL_Rect dst)
{
    SDL_Rect r; 

    r.x = dst.x + (dst.w - w) / 2;
    r.y = dst.y + (dst.h - h) / 2;
    r.w = w;
    r.h = h;

    SDL_RenderCopy(renderer,tex,NULL,&r);
}
