#include "action_string.h"
#include <stdio.h>

int action_string_size(void) 
{ 
    return 20; 
}

void action_string(const struct Action action, char *outstr) 
{
    sprintf(outstr,"%d %d %d %d %d %d %c",
            action.team,
            action.source.x, action.source.y,
            action.target.x, action.target.y,
            action.is_special, action.unit);
}

void action_print(const struct Action action, FILE *stream) 
{
    fprintf(stream,"%d %d %d %d %d %d %c",
            action.team,
            action.source.x, action.source.y,
            action.target.x, action.target.y,
            action.is_special, action.unit);
}

int action_string_size_verbose(void)
{
    return 55;
}

void action_string_verbose(const struct Action action, char *outstr)
{
    sprintf(outstr,
            "Team: %d\tUnit: %c\t%c\nSource: (%d,%d)\nTarget: (%d,%d)\n",
            action.team, action.unit, action.is_special?'!':'_',
            action.source.x, action.source.y,
            action.target.x, action.target.y);
}

void action_print_verbose(const struct Action action, FILE *stream)
{
    fprintf(stream,
            "Team: %d\tUnit: %c\t%c\nSource: (%d,%d)\nTarget: (%d,%d)\n",
            action.team, action.unit, action.is_special?'!':'_',
            action.source.x, action.source.y,
            action.target.x, action.target.y);
}

/* For debugging purposes. */
void action_list_print(const struct Action *actions, int num_actions, FILE *stream) 
{
    int i;
    for (i = 0; i < num_actions; ++i) {
        struct Action act = actions[i];
        fprintf(stream,
                "Team: %d\tUnit: %c\t%c\nSource: (%d,%d)\nTarget: (%d,%d)\n",
                act.team,act.unit,act.is_special?'!':'_',
                act.source.x,act.source.y,
                act.target.x,act.target.y);
    }
}
