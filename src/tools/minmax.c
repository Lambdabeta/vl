#include "minmax.h"

int minint(int a, int b)
{
    if (a < b) return a;
    return b;
}

int maxint(int a, int b)
{
    if (a > b) return a;
    return b;
}
