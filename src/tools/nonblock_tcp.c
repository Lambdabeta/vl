#include "nonblock_tcp.h"

#include <string.h>
#include <stdio.h>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

int create_nb_tcp_server(unsigned short *port)
{
    char portstr[6];
    struct addrinfo hints = {0};
    struct addrinfo *info;
    struct addrinfo *p;
    int ret;

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    sprintf(portstr,"%d",*port);

    if (getaddrinfo(NULL,portstr,&hints,&info)) {
        return -1;
    }

    for (p = info; p; p = p->ai_next) {
        if ((ret = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
            perror("socket");
            continue;
        }
        fcntl(ret,F_SETFL,O_NONBLOCK);
        if (bind(ret,p->ai_addr,p->ai_addrlen) < 0) {
            close(ret);
            perror("bind");
            continue;
        }
        if (listen(ret,5) < 0) {
            close(ret);
            perror("listen");
            continue;
        }

        if (info->ai_family == AF_INET) {
            struct sockaddr_in sin;
            socklen_t len = sizeof(struct sockaddr_in);
            if (getsockname(ret,(struct sockaddr *)&sin,&len) < 0) {
                close(ret);
                perror("getsockname");
                continue;
            }
            *port = ntohs(sin.sin_port);
        } else {
            struct sockaddr_in6 sin;
            socklen_t len = sizeof(struct sockaddr_in6);
            if (getsockname(ret,(struct sockaddr *)&sin,&len) < 0) {
                close(ret);
                perror("getsockname");
                continue;
            }
            *port = ntohs(sin.sin6_port);
        }

        break;
    }

    freeaddrinfo(info);
    return ret;
}

int nb_tcp_server_accept(int sockfd)
{
    int ret;
    struct sockaddr_storage addr;
    socklen_t addr_len;

    ret = accept(sockfd,(struct sockaddr *)&addr,&addr_len);

    if (ret < 0) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) return -1;
        perror("accept");
        return -1;
    }

    fcntl(ret,F_SETFL,O_NONBLOCK);

    return ret;
}

int create_nb_tcp_client(const char *ip, unsigned short port)
{
    char portstr[6];
    struct addrinfo hints = {0};
    struct addrinfo *info;
    void *addr;
    socklen_t len;
    int ret;

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    sprintf(portstr,"%d",port);

    if (getaddrinfo(ip,portstr,&hints,&info)) return -1;

    if (info->ai_family == AF_INET) {
        struct sockaddr_in *ipv4 = (struct sockaddr_in*)info->ai_addr;
        addr = ipv4;
        len = sizeof(struct sockaddr_in);
    } else {
        struct sockaddr_in6 *ipv6 = (struct sockaddr_in6*)info->ai_addr;
        addr = ipv6;
        len = sizeof(struct sockaddr_in6);
    }

    ret = socket(info->ai_family, info->ai_socktype, info->ai_protocol);

    if (ret  < 0) {
        freeaddrinfo(info);
        return -1;
    }

    fcntl(ret,F_SETFL,O_NONBLOCK);

    freeaddrinfo(info);
    if (connect(ret,(struct sockaddr*)addr,len) < 0) {
        if (errno == EINPROGRESS) return ret;
        return -1;
    }

    return ret;
}

int nb_tcp_send(int sockfd, const char *msg, size_t msg_len)
{
    int status = send(sockfd,msg,(msg_len?msg_len:strlen(msg)+1),0);
    if (status < 0) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) { return 0; }
        perror("send");
        return 0;
    }

    printf("Sent %d chars: %s\n",(msg_len?msg_len:strlen(msg)+1),msg);

    return status;
}

int nb_tcp_recv(int sockfd, char *buf, size_t bufsize)
{
    int status = recv(sockfd,buf,bufsize,0);
    if (status < 0) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) return 0;
        perror("recv");
        return -1;
    }

    if (status == 0) {
        printf("Disconnected\n");
        return -1;
    }
    printf("Received %d chars: %s\n",status,buf);

    return status;
}

void nb_tcp_close(int sockfd)
{
    close(sockfd);
}

