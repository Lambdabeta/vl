#ifndef TOOLS_STRING_INPUT_H
#define TOOLS_STRING_INPUT_H

#include <SDL2/SDL.h>

struct StringInput;

struct StringInput *string_input_new(SDL_Rect rect);
void string_input_process(struct StringInput *si, const SDL_Event *e);
void string_input_draw(const struct StringInput *si);
const char *string_input_string(const struct StringInput *si);
void string_input_free(struct StringInput *si);

#endif /* TOOLS_STRING_INPUT_H */
