#ifndef TOOLS_GAME_STRING_H
#define TOOLS_GAME_STRING_H

#include "../models/game.h"
#include <stdio.h>

/* Returns the size in characters of a string which will hold the representation
 * of game. */
int game_string_size(const struct Game game);

/* Fills the given buffer with the string representation of the game. */
void game_string(const struct Game game, char *outstr);

/* Prints the given game to the given stream using fprintf. */
void game_print(const struct Game game, FILE *stream);

/* TODO: also load from strings/streams */
#endif /* TOOLS_GAME_STRING_H */
