#ifndef TOOLS_DEBUG_H
#define TOOLS_DEBUG_H

#define DEBUG(...) do { printf("%s() %s:%d > ", __func__, __FILE__, __LINE__); printf(__VA_ARGS__); printf("\n"); fflush(stdout); } while (0)
#define ERROR(...) do { char buf[2048]; sprintf(buf,__VA_ARGS__); error_popup("ERROR",buf); } while (0)

void error_popup(const char *title, const char *msg);

#endif /* TOOLS_DEBUG_H */
