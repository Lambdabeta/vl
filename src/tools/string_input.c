#include "string_input.h"
#include <SDL2/SDL.h>

#include "font.h"
#include "centered.h"

#include "../res/fonts.h"
#include "../res/colours.h"
#include "../res/globals.h"

#define INIT_BUFSIZE 12

struct StringInput {
    char *buf; /* dynamically resized to hold input! */
    int bufsize;
    SDL_Rect rect;
    SDL_Texture *text;
    int w,h;
};

struct StringInput *string_input_new(SDL_Rect rect)
{
    struct StringInput *ret = malloc(sizeof(struct StringInput));
    ret->buf = malloc(sizeof(char) * INIT_BUFSIZE);
    ret->buf[0] = 0;
    ret->bufsize = INIT_BUFSIZE;
    ret->rect = rect;
    ret->text = NULL;

    SDL_StartTextInput();
    return ret;
}

static void redraw_si(struct StringInput *si) 
{
    if (si->text) { SDL_DestroyTexture(si->text); si->text = NULL; }
    if (strlen(si->buf)) { 
        si->text = render_text(font,si->buf,COL_STRING_INPUT_FG,&si->w,&si->h); 
    }
}

void string_input_process(struct StringInput *si, const SDL_Event *e)
{
    if (e->type == SDL_TEXTINPUT) {
        while (strlen(si->buf) + strlen(e->text.text) + 1 > si->bufsize) {
            char *newbuf = malloc(sizeof(char) * si->bufsize * 2);
            strcpy(newbuf,si->buf);
            si->bufsize *= 2;
            free(si->buf);
            si->buf = newbuf;
        }

        strcat(si->buf,e->text.text);
        redraw_si(si);
    } else if (e->type == SDL_KEYDOWN) {
        if (e->key.keysym.sym == SDLK_BACKSPACE && strlen(si->buf)) {
            si->buf[strlen(si->buf)-1] = 0;
            redraw_si(si);
        } else if (e->key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_CTRL) {
            SDL_SetClipboardText(si->buf);
        } else if (e->key.keysym.sym == SDLK_v && SDL_GetModState() & KMOD_CTRL) {
            si->buf = SDL_GetClipboardText();
            redraw_si(si);
        }
    }
}

void string_input_draw(const struct StringInput *si)
{
    SDL_SetRenderDrawColor(renderer,
            COL_STRING_INPUT_BG.r,
            COL_STRING_INPUT_BG.g,
            COL_STRING_INPUT_BG.b,
            COL_STRING_INPUT_BG.a);
    SDL_RenderFillRect(renderer,&si->rect);
    render_centered(si->text,si->w,si->h,si->rect);
}

const char *string_input_string(const struct StringInput *si)
{
    return si->buf;
}

void string_input_free(struct StringInput *si)
{
    free(si->buf);
    if (si->text) 
        SDL_DestroyTexture(si->text);
    free(si);
}
