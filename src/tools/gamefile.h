#ifndef TOOLS_GAMEFILE_H
#define TOOLS_GAMEFILE_H

#include "../models/game.h"

struct Game gamefile_load(const char *fname);

#endif /* TOOLS_GAMEFILE_H */
