#ifndef TOOLS_NONBLOCK_TCP_H
#define TOOLS_NONBLOCK_TCP_H

#include <stdlib.h>

/* <0 means no connection, try again. */
int create_nb_tcp_server(unsigned short *port); /* rewrites port as appropriate. */
int nb_tcp_server_accept(int sockfd);
int create_nb_tcp_client(const char *ip, unsigned short port);

/* Returns # bytes sent. If msg_len is 0, size will be inferred. */
int nb_tcp_send(int sockfd, const char *msg, size_t msg_len);

/* Returns # chars received. -1 == disconnect. */
int nb_tcp_recv(int sockfd, char *buf, size_t bufsize);

void nb_tcp_close(int sockfd);

#endif /* TOOLS_NONBLOCK_TCP_H */
