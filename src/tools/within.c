#include "within.h"

int within(int x, int y, SDL_Rect check)
{
    return (x > check.x && x - check.w < check.x) &&
           (y > check.y && y - check.h < check.y);
}
