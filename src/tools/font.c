#include "font.h"

#include "../res/globals.h"

SDL_Texture *render_text(TTF_Font *fnt, const char *str, SDL_Color col, int *w, int *h)
{
    SDL_Texture *ret;
    SDL_Surface *tmp = TTF_RenderText_Solid(fnt,str,col);

    if (!tmp) return NULL;

    if (w) *w = tmp->w;
    if (h) *h = tmp->h;

    ret = SDL_CreateTextureFromSurface(renderer,tmp);
    
    return ret;
}
