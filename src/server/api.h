#ifndef SERVER_API_H
#define SERVER_API_H

#include <stdlib.h>

#include "../models/server_game.h"

struct VL_API; /* Uses null-terminated strings as packets. */

/* Shallow copies game so it can be watched.
 * Receive must receive an entire packet, or return 0. -1 on disconnect.
 * Respond's integer indicates which player to respond to (1-indexed).
 * Each callback has its own data. */
struct VL_API *create_server(
        struct ServerGame *game,
        int (*receive)(char*,size_t,void*),void*d1,
        void (*respond)(const char*,int,void*),void*d2);

/* Shallow copies game so it can be watched. 
 * Receive must receive an entire packet, or return 0. -1 on disconnect.
 * Each callback has its own data. */
struct VL_API *create_client(
        struct LocalGame *game,
        int (*receive)(char*,size_t,void*),void*d1,
        void (*respond)(const char*,int,void*),void*d2); 

/* Returns winner or 0. Called automatically by all other api calls. 
 * Returns -1 on disconnect. (or before a connection has been made) */
int update_api(struct VL_API *);

/* Registers the given action, when possible. Won't show up as registered until
 * updated appropriately after server response. */
void register_api(struct VL_API *, struct Action);

/* Same as above, but unregisters. */
void unregister_api(struct VL_API *, struct Action);

/* Same as above, but commits. */
void commit_api(struct VL_API *);

/* Returns the appropriate local game for this api. */
struct LocalGame *game_api(struct VL_API *);

void *get_receive_data_api(struct VL_API *);

void *get_respond_data_apI(struct VL_API *);

#endif /* SERVER_API_H */

/* Server takes ASCII input: 
 *
 * Commands
 * ~~~~~~~~
 *
 * All commands begin with your player number, then a space, then the command.
 *
 * R sx sy tx ty sp un - Register action 
 * U sx sy tx ty sp un - Unregister action 
 * C - Commit actions 
 * ? - Query the board and valid actions list (not yet implemented)
 *
 * Responses
 * ~~~~~~~~~
 *
 * R sx sy tx ty sp un - This is a registered action.
 * U sx sy tx ty sp un - This is *not* a registered action.
 * V sx sy tx ty sp un - This is a valid action.
 * N # - Number of actions remaining.
 * C # - The player committed.
 * B w h {{team-type-unit}*\n}* - Lists the board. Please delete all registered
 * and valid actions on receive.
 * W # - The player won.
 */
