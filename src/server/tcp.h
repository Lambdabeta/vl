#ifndef SERVER_TCP_H
#define SERVER_TCP_H

#include "api.h"
#include "../models/game.h"

struct VL_API *create_tcp_server(struct Game game);
struct VL_API *create_tcp_client(const char *ip, unsigned short port);
void free_tcp(struct VL_API *);
unsigned short get_tcp_port(struct VL_API *);

#endif /* SERVER_TCP_H */
