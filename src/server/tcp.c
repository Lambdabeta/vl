#include "tcp.h"

#include "../tools/nonblock_tcp.h"
#include "../models/server_game.h"

#define MAX_BUFFER_SIZE 4096

#include <string.h>
#include <stdio.h>

struct VLTCP {
    unsigned short port;
    int serversock;
    int clientsock; 
    int is_connected;
    const char *ip;
};


static int vl_tcp_server_receive(char *buf, size_t bufsize, void *data)
{
    struct VLTCP *this = data;

    if (!this->is_connected) {
        this->clientsock = nb_tcp_server_accept(this->serversock);
        if (this->clientsock > 0) this->is_connected = 1;
    }

    if (this->is_connected) {
        int ret = nb_tcp_recv(this->clientsock,buf,bufsize);
        return ret;
    }
    return -1;
}

static void vl_tcp_server_respond(const char *buf, int team, void *data)
{
    struct VLTCP *this = data;

    if (this->is_connected)
        nb_tcp_send(this->clientsock,buf,0);
}

static int vl_tcp_client_receive(char *buf, size_t bufsize, void *data)
{
    struct VLTCP *this = data;

    if (!this->is_connected) {
        this->serversock = create_nb_tcp_client(this->ip,this->port);
        if (this->serversock > 0) this->is_connected = 1;
    }

    if (this->is_connected) {
        int ret = nb_tcp_recv(this->serversock,buf,bufsize);
        return ret;
    }

    return -1;
}

static void vl_tcp_client_respond(const char *buf, int team, void *data)
{
    struct VLTCP *this = data;

    if (this->is_connected)
        nb_tcp_send(this->serversock,buf,0);
}

struct VL_API *create_tcp_server(struct Game game)
{
    struct ServerGame *sg = malloc(sizeof(struct ServerGame));
    struct VLTCP *this = malloc(sizeof(struct VLTCP));
    this->port = 0;
    this->serversock = create_nb_tcp_server(&this->port);
    this->is_connected = 0;
    this->clientsock = -1;
    this->ip = NULL;
    generate_server_game(sg,game);
    return create_server(sg,vl_tcp_server_receive,this,
                            vl_tcp_server_respond,this);
}

struct VL_API *create_tcp_client(const char *ip, unsigned short port)
{
    struct LocalGame *lg = malloc(sizeof(struct LocalGame));
    struct VLTCP *this = malloc(sizeof(struct VLTCP));

    empty_local_game(lg);
    lg->player = 2;
    lg->remaining = 10000;

    this->port = port;
    this->serversock = create_nb_tcp_client(ip,port);
    this->is_connected = (this->serversock > 0);
    this->clientsock = -1;
    this->ip = ip;
    return create_client(lg,vl_tcp_client_receive,this,
                            vl_tcp_client_respond,this);
}

void free_tcp(struct VL_API *api)
{
    struct VLTCP *this = get_receive_data_api(api);
    nb_tcp_close(this->serversock);
    if (this->clientsock > 0) nb_tcp_close(this->clientsock);
    free(this);
}

unsigned short get_tcp_port(struct VL_API *api)
{
    struct VLTCP *this = get_receive_data_api(api);
    return this->port;
}

