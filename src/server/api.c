#include "api.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include "../tools/game_string.h"
#include "../tools/debug.h"

#define API_BUFFER_SIZE 2048

struct VL_API {
    int is_server;

    union {
        struct ServerGame *game;
        struct LocalGame *local;
    };

    int (*receive)(char*,size_t,void*); void *receive_data;
    void (*respond)(const char *,int,void*); void *respond_data;
};

struct VL_API *create_server(
        struct ServerGame *game,
        int (*receive)(char*,size_t,void*),void*d1,
        void (*respond)(const char*,int,void*),void*d2)
{
    struct VL_API *ret = malloc(sizeof(struct VL_API));
    ret->is_server = 1;
    ret->game = game;
    ret->receive = receive; ret->receive_data = d1;
    ret->respond = respond; ret->respond_data = d2;
    return ret;
}

struct VL_API *create_client(
        struct LocalGame *game,
        int (*receive)(char*,size_t,void*),void*d1,
        void (*respond)(const char*,int,void*),void*d2)
{
    struct VL_API *ret = malloc(sizeof(struct VL_API));
    ret->is_server = 0;
    ret->local = game;
    ret->receive = receive; ret->receive_data = d1;
    ret->respond = respond; ret->respond_data = d2;
    return ret;
}

/* Takes string, output team, output command, returns start of command data. */
static char *server_sanitize_data(char *data, int *team, char *command)
{
    if (strlen(data) < 3) return NULL;
    *team = data[0] - '0';
    *command = data[2];

    return data + 4;
}

static void sendR(struct VL_API *this, struct Action action)
{
    char str[25]; /* R ### ### ### ### # C */

    sprintf(str,"R %d %d %d %d %d %c",
            action.source.x, action.source.y,
            action.target.x, action.target.y,
            action.is_special, action.unit);

    this->respond(str,action.team,this->respond_data);
}

static void sendU(struct VL_API *this, struct Action action)
{
    char str[25]; /* U ### ### ### ### # C */

    sprintf(str,"U %d %d %d %d %d %c",
            action.source.x, action.source.y,
            action.target.x, action.target.y,
            action.is_special, action.unit);

    this->respond(str,action.team,this->respond_data);
}

static void sendN(struct VL_API *this, int team)
{
    char str[6]; /* N ### */

    sprintf(str,"N %d", this->game->locals[team-1].remaining);

    this->respond(str,team,this->respond_data);
}

static void sendB(struct VL_API *this, int team)
{
    int bindex;
    char *buf = malloc(sizeof(char)*(game_string_size(this->game->locals[team-1].game) + 8));
    
    bindex = sprintf(buf,"B %d %d ", this->game->locals[team-1].game.w,
                                     this->game->locals[team-1].game.h);
    game_string(this->game->locals[team-1].game,buf+bindex);

    this->respond(buf,team,this->respond_data);
    free(buf);
}

static void sendAllV(struct VL_API *this, int team)
{
    int i;
    char buf[25];
    for (i = 0; i < this->game->locals[team-1].num_valid; ++i) {
        struct Action action = this->game->locals[team-1].valid[i];
        sprintf(buf, "V %d %d %d %d %d %c",
            action.source.x, action.source.y,
            action.target.x, action.target.y,
            action.is_special, action.unit);
        this->respond(buf,action.team,this->respond_data);
    }
}

static void broadcastUpdate(struct VL_API *this, int team)
{
    /* TODO: calculate number of players, probably using game? */
    sendB(this,2);
    sendAllV(this,2);
}

static void broadcastCommit(struct VL_API *this, int team)
{
    /* TODO: calculate number of players, probably using game? */
    char buf[10];
    sprintf(buf, "C %d", team);
    this->respond(buf,team,this->respond_data);
}

static void server_register(struct VL_API *this, struct Action action)
{
    struct LocalGame *lg = &this->game->locals[action.team-1];
    int num_registered = lg->num_registered;

    if (!this->is_server)
        force_register_local_game(lg,action);
    else
        register_local_game(lg,action);
    if (num_registered < lg->num_registered) {
        sendR(this,action);
        sendN(this,action.team);
    }
}

static void server_unregister(struct VL_API *this, struct Action action)
{
    struct LocalGame *lg = &this->game->locals[action.team-1];
    int num_registered = lg->num_registered;

    unregister_local_game(lg,action);
    if (num_registered > lg->num_registered) {
        sendU(this,action);
        sendN(this,action.team);
    }
}

static int server_process_register(struct VL_API *this, int team, char *data)
{
    struct Action action;
    char unit;
    sscanf(data,"%d %d %d %d %d %c",
           &action.source.x, &action.source.y,
           &action.target.x, &action.target.y,
           &action.is_special, &unit);
    action.unit = unit;
    action.team = team;

    server_register(this,action);
    return 0;
}

static int server_process_unregister(struct VL_API *this, int team, char *data)
{
    struct Action action;
    char unit;
    sscanf(data,"%d %d %d %d %d %c",
           &action.source.x, &action.source.y,
           &action.target.x, &action.target.y,
           &action.is_special, &unit);
    action.unit = unit;
    action.team = team;

    server_unregister(this,action);
    return 0;
}

static int server_process_commit(struct VL_API *this, int team, char *data)
{
    int res = commit_server_game(this->game,team-1);   
    if (res < 0) return -res;
    broadcastCommit(this,team);
    if (res) 
        broadcastUpdate(this,1);
    return 0;
}

static int server_process_update(struct VL_API *this, int team, char *data)
{
    broadcastUpdate(this,team);
    return 0;
}

static int server_process_received_data(struct VL_API *this, char *data)
{
    int team;
    char command;
    char *contents;
    if (!(contents = server_sanitize_data(data,&team,&command))) {
        DEBUG("Invalid contents: %s",data);
        return 0;
    }

    switch (command) {
        case 'R': return server_process_register(this,team,contents);
        case 'U': return server_process_unregister(this,team,contents);
        case 'C': return server_process_commit(this,team,contents);
        case '?': return server_process_update(this,team,contents);
        default: DEBUG("Unknown command: %c",command); return 0;
    }
}

static int client_process_register(struct VL_API *this, char *data)
{
    struct Action action;
    char unit;
    sscanf(data,"%d %d %d %d %d %c",
           &action.source.x, &action.source.y,
           &action.target.x, &action.target.y,
           &action.is_special, &unit);
    action.unit = unit;
    action.team = 2; /* TODO: load in somehow? */

    force_register_local_game(this->local,action);
    return 0;
}

static int client_process_unregister(struct VL_API *this, char *data)
{
    struct Action action;
    char unit;
    sscanf(data,"%d %d %d %d %d %c",
           &action.source.x, &action.source.y,
           &action.target.x, &action.target.y,
           &action.is_special, &unit);
    action.unit = unit;
    action.team = 2; /* TODO: load in somehow? */

    unregister_local_game(this->local,action);
    return 0;
}

static int client_process_valid(struct VL_API *this, char *data)
{
    struct Action action;
    char unit;
    sscanf(data,"%d %d %d %d %d %c",
           &action.source.x, &action.source.y,
           &action.target.x, &action.target.y,
           &action.is_special, &unit);
    action.unit = unit;
    action.team = 2; /* TODO: load in somehow? */

    add_valid_local_game(this->local,action);
    return 0;
}

static int client_process_remaining(struct VL_API *this, char *data)
{
    int numR = atoi(data);

    this->local->remaining = numR;
    return 0;
}

static int client_process_board(struct VL_API *this, char *data)
{
    int w,h,x,y;
    int consumed;
    char *input = data;
    sscanf(input,"%d %d%n",&w,&h,&consumed);
    input += consumed + 1; /* check for oboes */
    
    if (!(this->local->game.tiles && this->local->game.w == w && this->local->game.h == h)) {
        int i;
        free_game(this->local->game);
        this->local->game.w = w;
        this->local->game.h = h;
        this->local->game.tiles = malloc(sizeof(struct Tile*) * h);
        for (i = 0; i < h; ++i)
            this->local->game.tiles[i] = malloc(sizeof(struct Tile) * w);
    }

    for (y = 0; y < h; ++y) {
        for (x = 0; x < w; ++x) {
            this->local->game.tiles[y][x].team = *input++ - '0';
            this->local->game.tiles[y][x].type = *input++;
            this->local->game.tiles[y][x].unit = *input++; 
        }
        input++;
    }

    clear_actions_local_game(this->local);
    return 0;
}

static int client_process_winner(struct VL_API *this, char *data)
{
    return atoi(data);
}

static int client_process_commit(struct VL_API *this, char *data)
{
    if (this->local->player == atoi(data)) {
        this->local->remaining = -1;
    }
    return 0;
}

/* Takes string, output team, output command, returns start of command data. */
static char *client_sanitize_data(char *data, char *command)
{
    if (strlen(data) < 1) return NULL;
    *command = data[0];

    return data + 2;
}

static int client_process_received_data(struct VL_API *this, char *data)
{
    char command;
    char *contents;
    if (!(contents = client_sanitize_data(data,&command))) {
        DEBUG("Invalid contents: %s",data);
        return 0;
    }

    switch (command) {
        case 'R': return client_process_register(this,contents);
        case 'U': return client_process_unregister(this,contents);
        case 'V': return client_process_valid(this,contents);
        case 'N': return client_process_remaining(this,contents);
        case 'B': return client_process_board(this,contents);
        case 'W': return client_process_winner(this,contents);
        case 'C': return client_process_commit(this,contents);
        default: DEBUG("Unknown command: %c",command); return 0;
    }
}

static int process_received_data(struct VL_API *this, char *data, int num_recved)
{
    int ret = 0;
    while (num_recved > 0) {
        int datalen = strlen(data);
        if (this->is_server)
            ret |= server_process_received_data(this,data);
        else
            ret |= client_process_received_data(this,data);
        data += datalen+1;
        num_recved -= datalen+1;
    }
    return ret;
}

int update_api(struct VL_API *this)
{
    char buf[API_BUFFER_SIZE];
    int status;
    if ((status = this->receive(buf,API_BUFFER_SIZE,this->receive_data))) {
        if (status < 0) return -1;
        return process_received_data(this,buf,status);
    }

    if (this->is_server) {
        return 0;
    } else {
        if (this->local->game.tiles) return 0;
        this->respond("2 ?",2,this->respond_data);
        return -1;
    }
}

void register_api(struct VL_API *this, struct Action action)
{
    if (this->is_server) {
        register_local_game(&this->game->locals[0],action);
    } else {
        char buf[25];
        sprintf(buf,"2 R %d %d %d %d %d %c",
                action.source.x,
                action.source.y,
                action.target.x,
                action.target.y,
                action.is_special,
                action.unit);
        this->respond(buf,2,this->respond_data);
    }
}

void unregister_api(struct VL_API *this, struct Action action)
{
    if (this->is_server) {
        unregister_local_game(&this->game->locals[0],action);
    } else {
        char buf[25];
        sprintf(buf,"2 U %d %d %d %d %d %c",
                action.source.x,
                action.source.y,
                action.target.x,
                action.target.y,
                action.is_special,
                action.unit);
        this->respond(buf,2,this->respond_data);
    }
}

void commit_api(struct VL_API *this)
{
    if (this->is_server) {
        int status = commit_server_game(this->game,0);
        if (status) {
            if (status < 0) {
                if (status == -1) 
                    this->respond("W 1",2,this->respond_data);
                else
                    this->respond("W 2",2,this->respond_data);
            } else {
                broadcastUpdate(this,2);
            }            
        }
    } else {
        this->respond("2 C",2,this->respond_data);
    }
}

struct LocalGame *game_api(struct VL_API *this)
{
    if (this->is_server) {
        return &this->game->locals[0];
    } else {
        return this->local;
    }
}

void *get_receive_data_api(struct VL_API *this)
{
    return this->receive_data;
}

void *get_respond_data_api(struct VL_API *this)
{
    return this->respond_data;
}

