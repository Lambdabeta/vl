#ifndef MODELS_SERVER_GAME_H
#define MODELS_SERVER_GAME_H

#include "local_game.h"

struct ServerGame {
    struct Game game;
    struct LocalGame *locals;
    int *committed;
}; 

void generate_server_game(struct ServerGame *server, struct Game game);

/* Returns 1 if it causes an update. 
 * If a player wins, returns their number, but negative. */
int commit_server_game(struct ServerGame *server, int player);

#endif /* MODELS_SERVER_GAME_H */
