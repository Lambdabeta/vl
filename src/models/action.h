#ifndef MODELS_ACTION_H
#define MODELS_ACTION_H

#include "tile.h"

struct Coord {
    int x,y;
};

struct Action {
    /* For spawn source==target, is_special==false */
    struct Coord source;
    struct Coord target;

    int is_special;
    enum UnitType unit;
    int team;
};


#include "game.h"

/* Allocates a new list of actions in the specified pointer, returning its
 * length. If the specified pointer is null, just returns length. The player
 * indicates whose valid actions to return, 0 for everyones. The list is
 * malloc'd and must be free'd. */
int list_actions(const struct Game *game, int player, /*@out@*/struct Action **list);

/* returns the number of actions left to the given player, player of 0 indicates
 * to calculate maximum possible actions the game could have. */
int num_actions(const struct Game *game, int player);

/* Returns 1 if the specified action is a spawn action. */
int action_is_spawn(const struct Action action);

/* Returns 1 if the specified action is a movement action. */
int action_is_move(const struct Action action);

#endif /* MODELS_ACTION_H */
