#include "tile.h"

static enum UnitType matchups[] = {
/* 0b00000 */ UNIT_TYPE_NONE,
/* 0b00001 */ UNIT_TYPE_ZOMBIE,
/* 0b00010 */ UNIT_TYPE_FAIRY,
/* 0b00011 */ UNIT_TYPE_ZOMBIE,
/* 0b00100 */ UNIT_TYPE_HUMAN,
/* 0b00101 */ UNIT_TYPE_ZOMBIE,
/* 0b00110 */ UNIT_TYPE_HUMAN,
/* 0b00111 */ UNIT_TYPE_ZOMBIE,
/* 0b01000 */ UNIT_TYPE_LEPRECHAUN,
/* 0b01001 */ UNIT_TYPE_LEPRECHAUN,
/* 0b01010 */ UNIT_TYPE_FAIRY,
/* 0b01011 */ UNIT_TYPE_NONE,
/* 0b01100 */ UNIT_TYPE_HUMAN,
/* 0b01101 */ UNIT_TYPE_NONE,
/* 0b01110 */ UNIT_TYPE_HUMAN,
/* 0b01111 */ UNIT_TYPE_NONE,
/* 0b10000 */ UNIT_TYPE_VAMPIRE,
/* 0b10001 */ UNIT_TYPE_VAMPIRE,
/* 0b10010 */ UNIT_TYPE_FAIRY,
/* 0b10011 */ UNIT_TYPE_NONE,
/* 0b10100 */ UNIT_TYPE_VAMPIRE,
/* 0b10101 */ UNIT_TYPE_VAMPIRE,
/* 0b10110 */ UNIT_TYPE_NONE,
/* 0b10111 */ UNIT_TYPE_NONE,
/* 0b11000 */ UNIT_TYPE_LEPRECHAUN,
/* 0b11001 */ UNIT_TYPE_LEPRECHAUN,
/* 0b11010 */ UNIT_TYPE_FAIRY,
/* 0b11011 */ UNIT_TYPE_NONE,
/* 0b11100 */ UNIT_TYPE_NONE,
/* 0b11101 */ UNIT_TYPE_NONE,
/* 0b11110 */ UNIT_TYPE_NONE,
/* 0b11111 */ UNIT_TYPE_NONE
};

static int check_mask(unsigned int mask, enum UnitType unit) 
{
    switch (unit) {
        case UNIT_TYPE_VAMPIRE: return mask&0x10;
        case UNIT_TYPE_LEPRECHAUN: return mask&0x8;
        case UNIT_TYPE_HUMAN: return mask&0x4;
        case UNIT_TYPE_FAIRY: return mask&0x2;
        case UNIT_TYPE_ZOMBIE: return mask&0x1;
        default: return 0;
    }
}

static void set_mask(unsigned int *mask, enum UnitType unit)
{
    switch (unit) {
        case UNIT_TYPE_VAMPIRE: *mask|=0x10; break;
        case UNIT_TYPE_LEPRECHAUN: *mask|=0x8; break;
        case UNIT_TYPE_HUMAN: *mask|=0x4; break;
        case UNIT_TYPE_FAIRY: *mask|=0x2; break;
        case UNIT_TYPE_ZOMBIE: *mask|=0x1; break;
        case UNIT_TYPE_UNKNOWN:
        case UNIT_TYPE_NONE: break;
    }
}

static void load_present_and_duplicated(enum UnitType *units, unsigned int *present, unsigned int *duplicated)
{
    for (*present = *duplicated = 0; *units != UNIT_TYPE_NONE; ++units) {
        if (check_mask(*present,*units)) 
            set_mask(duplicated,*units);
        else
            set_mask(present,*units);
    }
}

void resolve_tile(struct Tile *tile, enum UnitType *units, int *teams, int create)
{
    /* bitmasks as VLHFZ */
    unsigned int present_units = 0;
    unsigned int duplicated_units = 0;
    enum UnitType victor;
    if (create && tile->type == TILE_TYPE_PIT) tile->type = TILE_TYPE_FLOOR;

    load_present_and_duplicated(units,&present_units,&duplicated_units);
    
    set_mask(&present_units,tile->unit);

    victor = matchups[present_units];

    if (check_mask(duplicated_units,victor)) {
        tile->unit = UNIT_TYPE_NONE;
        tile->team = 0;
    } else {
        int i;
        tile->unit = victor;
        for (i = 0; units[i] != UNIT_TYPE_NONE; ++i) {
            if (units[i] == victor) {
                tile->team = teams[i]; 
                break;
            }
        }
        /* Note: if the for loop did not find anything, then the original unit
         * was the victor and the team can stay the same. */
    }

    if (tile->type == TILE_TYPE_PIT && tile->unit != UNIT_TYPE_FAIRY) {
        tile->unit = UNIT_TYPE_NONE;
        tile->team = 0;
    }
}

void destroy_tile(struct Tile *tile)
{
    tile->type = TILE_TYPE_PIT;
}

void clear_tile(struct Tile *tile)
{
    tile->unit = UNIT_TYPE_NONE;
    if (tile->type != TILE_TYPE_BASE) tile->team = 0;
}
