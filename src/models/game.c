#include "game.h"

#include <stdlib.h>

#include "../res/ints.h"

void free_game(/*@null@*/struct Game g)
{
    int y;
    for (y = 0; y < g.h; ++y) free(g.tiles[y]);
    free(g.tiles);
    g.tiles = NULL;
}

static void reset_units_array(/*@out@*/enum UnitType units[INT_MAX_COLLISION+1],
                              /*@out@*/int teams[INT_MAX_COLLISION+1],
                              /*@unused@*/struct Tile current)
{
    int i;
    for (i = 0; i < INT_MAX_COLLISION; ++i) {
        units[i] = UNIT_TYPE_NONE;
        teams[i] = 0;
    }
}

static void resolve_coordinate(struct Game *game, int x, int y, struct Action *actions, int num_actions) 
{
    enum UnitType units[INT_MAX_COLLISION+1];
    int teams[INT_MAX_COLLISION+1];
    int create,i,a;
    struct Tile *current = &game->tiles[y][x];

    reset_units_array(units,teams,game->tiles[y][x]);
    i = 0; 
    create = 0;
    
    for (a = 0; a < num_actions; ++a) {
        struct Action act = actions[a];
        if (act.source.x == x && act.source.y == y && action_is_move(act)) {
            current->unit = UNIT_TYPE_NONE;
            current->team = 0;
        }
        if (act.target.x == x && act.target.y == y) {
            if (act.is_special) {
                switch (act.unit) {
                    case UNIT_TYPE_LEPRECHAUN: create = 1; break;
                    case UNIT_TYPE_HUMAN: destroy_tile(current); break;
                    case UNIT_TYPE_ZOMBIE: 
                        units[i] = UNIT_TYPE_ZOMBIE;
                        teams[i++] = act.team;
                        break;
                    default: break;
                }
            } else {
                units[i] = act.unit;
                teams[i++] = act.team;
            }
        }
    }

    units[i] = UNIT_TYPE_NONE;

    resolve_tile(current,units,teams,create);
}

int game_resolve_actions(struct Game *game, struct Action *actions, int num_actions)
{
    /* TODO: sanity check to prevent cheating! */
    int x,y;

    for (y = 0; y < game->h; ++y)
        for (x = 0; x < game->w; ++x)
            resolve_coordinate(game,x,y,actions,num_actions);

    return 0; /* TODO: return winner if exists. */
}

#define ABS(X) ((X) < 0 ? -(X) : (X))

static int is_visible(int x, int y, struct Game *src, int team)
{
    int dx, dy;
    if (!team) return 1;
    
    for (dy = -INT_MAX_VIS; dy <= INT_MAX_VIS; ++dy) {
        for (dx = -INT_MAX_VIS; dx <= INT_MAX_VIS; ++dx) {
            if (y+dy < 0 || y+dy >= src->h || x+dx < 0 || x+dx >= src->w)
                continue;

            if (src->tiles[y+dy][x+dx].team == team) {
                switch (src->tiles[y+dy][x+dx].unit) {
                    case UNIT_TYPE_VAMPIRE:
                        if (ABS(dx) <= INT_VAMPIRE_VIS &&
                            ABS(dy) <= INT_VAMPIRE_VIS) return 1;
                        break;
                    case UNIT_TYPE_LEPRECHAUN:
                        if (ABS(dx) <= INT_LEPRECHAUN_VIS &&
                            ABS(dy) <= INT_LEPRECHAUN_VIS) return 1;
                        break;
                    case UNIT_TYPE_HUMAN:
                        if (ABS(dx) <= INT_HUMAN_VIS &&
                            ABS(dy) <= INT_HUMAN_VIS) return 1;
                        break;
                    case UNIT_TYPE_ZOMBIE:
                        if (ABS(dx) <= INT_ZOMBIE_VIS &&
                            ABS(dy) <= INT_ZOMBIE_VIS) return 1;
                        break;
                    case UNIT_TYPE_FAIRY:
                        if (ABS(dx) <= INT_FAIRY_VIS &&
                            ABS(dy) <= INT_FAIRY_VIS) return 1;
                        break;
                    default: break;
                }
            }
        }
    }

    return 0;
}

struct Game game_local(struct Game src, int team)
{
    int x,y;
    struct Game ret;

    ret.tiles = malloc(sizeof(struct Tile *) * src.h);
    if (!ret.tiles) {
        ret.w = ret.h = -1;
        return ret;
    }
    for (y = 0; y < src.h; ++y) 
        ret.tiles[y] = malloc(sizeof(struct Tile) * src.w);
    ret.w = src.w;
    ret.h = src.h;

    for (y = 0; y < src.h; ++y) {
        for (x = 0; x < src.w; ++x) {
            ret.tiles[y][x] = src.tiles[y][x];
            if (!is_visible(x,y,&src,team)) {
                ret.tiles[y][x].unit = UNIT_TYPE_UNKNOWN;
                ret.tiles[y][x].type = TILE_TYPE_UNKNOWN;
                ret.tiles[y][x].team = 0;
            }
        }
    }

    return ret;
}

