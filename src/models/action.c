#include "action.h"

#include <stdlib.h>
#include <stdio.h>

static int max_actions(const struct Game *game, /*@unused@*/int player)
{
    int x,y;
    int ret = 0;
    
    for (y = 0; y < game->h; ++y) {
        for (x = 0; x < game->w; ++x) {
            if (game->tiles[y][x].type == TILE_TYPE_BASE)
                ret += 20;
            if (game->tiles[y][x].unit != UNIT_TYPE_NONE)
                ret += 16;
        }
    }
    
    return ret;
}

static void add_act(/*@out@*/struct Action *ret, int *i, const struct Game *game,
        int sx, int sy, int dx, int dy, int sp, enum UnitType unit, int team)
{
    if (sx >= 0 && sx < game->w && sy >= 0 && sy < game->h &&
        dx >= 0 && dx < game->w && dy >= 0 && dy < game->h) {
        ret[*i].source.x = sx; ret[*i].source.y = sy;
        ret[*i].target.x = dx; ret[*i].target.y = dy;
        ret[*i].is_special = sp; ret[*i].unit = unit;
        ret[*i].team = team; (*i)++;
    }
}

static void add_spawns(/*@out@*/struct Action *ret, int *i, const struct Game *game, int x, int y)
{
    /* North */
    add_act(ret,i,game,x,y-1,x,y-1,0,UNIT_TYPE_VAMPIRE,game->tiles[y][x].team);
    add_act(ret,i,game,x,y-1,x,y-1,0,UNIT_TYPE_LEPRECHAUN,game->tiles[y][x].team);
    add_act(ret,i,game,x,y-1,x,y-1,0,UNIT_TYPE_HUMAN,game->tiles[y][x].team);
    add_act(ret,i,game,x,y-1,x,y-1,0,UNIT_TYPE_FAIRY,game->tiles[y][x].team);
    add_act(ret,i,game,x,y-1,x,y-1,0,UNIT_TYPE_ZOMBIE,game->tiles[y][x].team);
    /* South */
    add_act(ret,i,game,x,y+1,x,y+1,0,UNIT_TYPE_VAMPIRE,game->tiles[y][x].team);
    add_act(ret,i,game,x,y+1,x,y+1,0,UNIT_TYPE_LEPRECHAUN,game->tiles[y][x].team);
    add_act(ret,i,game,x,y+1,x,y+1,0,UNIT_TYPE_HUMAN,game->tiles[y][x].team);
    add_act(ret,i,game,x,y+1,x,y+1,0,UNIT_TYPE_FAIRY,game->tiles[y][x].team);
    add_act(ret,i,game,x,y+1,x,y+1,0,UNIT_TYPE_ZOMBIE,game->tiles[y][x].team);
    /* East */
    add_act(ret,i,game,x+1,y,x+1,y,0,UNIT_TYPE_VAMPIRE,game->tiles[y][x].team);
    add_act(ret,i,game,x+1,y,x+1,y,0,UNIT_TYPE_LEPRECHAUN,game->tiles[y][x].team);
    add_act(ret,i,game,x+1,y,x+1,y,0,UNIT_TYPE_HUMAN,game->tiles[y][x].team);
    add_act(ret,i,game,x+1,y,x+1,y,0,UNIT_TYPE_FAIRY,game->tiles[y][x].team);
    add_act(ret,i,game,x+1,y,x+1,y,0,UNIT_TYPE_ZOMBIE,game->tiles[y][x].team);
    /* West */
    add_act(ret,i,game,x-1,y,x-1,y,0,UNIT_TYPE_VAMPIRE,game->tiles[y][x].team);
    add_act(ret,i,game,x-1,y,x-1,y,0,UNIT_TYPE_LEPRECHAUN,game->tiles[y][x].team);
    add_act(ret,i,game,x-1,y,x-1,y,0,UNIT_TYPE_HUMAN,game->tiles[y][x].team);
    add_act(ret,i,game,x-1,y,x-1,y,0,UNIT_TYPE_FAIRY,game->tiles[y][x].team);
    add_act(ret,i,game,x-1,y,x-1,y,0,UNIT_TYPE_ZOMBIE,game->tiles[y][x].team);
}

static void add_all_cardinal(/*@out@*/struct Action *ret, int *i, const struct Game *game, int x, int y, 
        int sp, enum UnitType unit, int team)
{
    add_act(ret,i,game,x,y,x,y-1,sp,unit,team);
    add_act(ret,i,game,x,y,x+1,y-1,sp,unit,team);
    add_act(ret,i,game,x,y,x+1,y,sp,unit,team);
    add_act(ret,i,game,x,y,x+1,y+1,sp,unit,team);
    add_act(ret,i,game,x,y,x,y+1,sp,unit,team);
    add_act(ret,i,game,x,y,x-1,y+1,sp,unit,team);
    add_act(ret,i,game,x,y,x-1,y,sp,unit,team);
    add_act(ret,i,game,x,y,x-1,y-1,sp,unit,team);
}

static void add_special(/*@out@*/struct Action *ret, int *i, const struct Game *game, int x, int y)
{
    enum UnitType unit = game->tiles[y][x].unit;
    int team = game->tiles[y][x].team;
    switch (unit) {
        case UNIT_TYPE_VAMPIRE:
            if (game->tiles[y][x].type == TILE_TYPE_PRODUCER) return;
            add_act(ret,i,game,x,y,x,y-2,0,unit,team);
            add_act(ret,i,game,x,y,x,y+2,0,unit,team);
            add_act(ret,i,game,x,y,x-2,y,0,unit,team);
            add_act(ret,i,game,x,y,x+2,y,0,unit,team);
            /*@fallthrough@*/
        case UNIT_TYPE_HUMAN:
        case UNIT_TYPE_LEPRECHAUN:
        case UNIT_TYPE_ZOMBIE:
            add_all_cardinal(ret,i,game,x,y,1,unit,team);
            /*@fallthrough@*/
        default: break;
    }
}

int list_actions(const struct Game *game, int player, /*@out@*/struct Action **list)
{
    int x,y,i;
    struct Action *ret = malloc(sizeof(struct Action) * max_actions(game,player));
    if (!ret) {
        *list = NULL;
        return -1;
    }

    i = 0;
    for (y = 0; y < game->h; ++y) {
        for (x = 0; x < game->w; ++x) {
            if (game->tiles[y][x].type == TILE_TYPE_BASE)
                add_spawns(ret,&i,game,x,y);
            else if (game->tiles[y][x].unit != UNIT_TYPE_NONE &&
                game->tiles[y][x].unit != UNIT_TYPE_UNKNOWN) {
                add_special(ret,&i,game,x,y);
                if (game->tiles[y][x].type != TILE_TYPE_PRODUCER)
                    add_all_cardinal(ret,&i,game,x,y,0,
                            game->tiles[y][x].unit,game->tiles[y][x].team);
            }
        }
    }

    *list = ret;

    return i;
}

int num_actions(const struct Game *game, int player)
{
    int x,y;
    int ret = 0;

    for (y = 0; y < game->h; ++y) {
        for (x = 0; x < game->w; ++x) {
            if ((game->tiles[y][x].type == TILE_TYPE_BASE || 
                 game->tiles[y][x].type == TILE_TYPE_PRODUCER) &&
                (game->tiles[y][x].team == player || 
                 player == 0)) ++ret;
        }
    }

    return ret;
}

int action_is_spawn(const struct Action action) 
{
    return action.source.x == action.target.x &&
           action.source.y == action.target.y;
}

int action_is_move(const struct Action action)
{
    return (action.source.x != action.target.x ||
           action.source.y != action.target.y ) &&
           !action.is_special;
}
