#ifndef MODELS_TILE_H
#define MODELS_TILE_H

enum TileType {
    TILE_TYPE_BASE = '#',
    TILE_TYPE_PIT = 'X',
    TILE_TYPE_FLOOR = ' ',
    TILE_TYPE_PRODUCER = '$',
    TILE_TYPE_UNKNOWN = '?'
};

enum UnitType {
    UNIT_TYPE_VAMPIRE = 'V',
    UNIT_TYPE_LEPRECHAUN = 'L',
    UNIT_TYPE_HUMAN = 'H',
    UNIT_TYPE_FAIRY = 'F',
    UNIT_TYPE_ZOMBIE = 'Z',
    UNIT_TYPE_NONE = ' ',
    UNIT_TYPE_UNKNOWN = '?'
};

struct Tile {
    enum TileType type;
    enum UnitType unit; /* base is type base, unit none, team of base */
    int team; /* NOTE: 1-based */
};

/*  Units is a NONE terminated list of units. Teams is a list of teams
 *  associated with units. Create is 1 if the tile is to be created, 0
 *  otherwise. */
void resolve_tile(struct Tile *tile, enum UnitType *units, int *teams, int create);

/* Sets tile as destroyed, DOES NOT FREE TILE DATA! */
void destroy_tile(struct Tile *tile);

/* Clears the tile, removing all occupants. */
void clear_tile(struct Tile *tile);

#endif /* MODELS_TILE_H */
