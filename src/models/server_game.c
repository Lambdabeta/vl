#include "server_game.h"

#include <stdlib.h>
#include <string.h>

void generate_server_game(struct ServerGame *server, struct Game game)
{
    server->game = game;

    server->locals = malloc(sizeof(struct LocalGame) * 2);
    generate_local_game(&server->locals[0], game, 1);
    generate_local_game(&server->locals[1], game, 2);

    server->committed = malloc(sizeof(int) * 2);
    server->committed[0] = 0;
    server->committed[1] = 0;
}

int commit_server_game(struct ServerGame *server, int player)
{
    server->committed[player] = 1;
    server->locals[player].remaining = -1;
    if (server->committed[0] && server->committed[1]) {
        struct Action *all_actions;
        int num_actions;
        int winner;

        server->committed[0] = 0;
        server->committed[1] = 0;

        num_actions = server->locals[0].num_registered +
                      server->locals[1].num_registered;
        all_actions = malloc(sizeof(struct Action) * num_actions);
        memcpy(all_actions,server->locals[0].registered,
                sizeof(struct Action) * server->locals[0].num_registered);
        memcpy(all_actions + server->locals[0].num_registered, 
                server->locals[1].registered,
                sizeof(struct Action) * server->locals[1].num_registered);

        winner = game_resolve_actions(&server->game, all_actions, num_actions);

        free_local_game(&server->locals[0]);
        free_local_game(&server->locals[1]);
        generate_local_game(&server->locals[0],server->game,1);
        generate_local_game(&server->locals[1],server->game,2);

        if (winner) 
            return -winner;
        return 1;
    }
    return 0;
}

