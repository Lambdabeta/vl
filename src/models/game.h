#ifndef MODELS_GAME_H
#define MODELS_GAME_H

#include "tile.h"

struct Game {
    struct Tile **tiles; /* [y][x] indexed. */
    int w,h;
};

#include "action.h"

void free_game(/*@null@*/struct Game g);

/* Actions is a NULL terminated list of actions. Returns winner #, 0 if no
 * winner. */
int game_resolve_actions(struct Game *game, struct Action *actions, int num_actions);

/* Creates a brand new game, localized to the given player. Player 0 can see
 * everything! (game_local(x,0) is analogous to game_clone) */
struct Game game_local(struct Game src, int team);

#endif /* MODELS_GAME_H */


