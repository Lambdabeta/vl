#ifndef MODELS_LOCAL_GAME_H
#define MODELS_LOCAL_GAME_H

#include "game.h"

struct LocalGame {
    struct Game game;
    struct Action *valid; int num_valid;
    struct Action *registered; int num_registered;
    int remaining; /* -1 === committed */
    int player;
};

void free_local_game(struct LocalGame *local);

void generate_local_game(struct LocalGame *local, struct Game game, int team);

void register_local_game(struct LocalGame *local, struct Action act);

void force_register_local_game(struct LocalGame *local, struct Action act);

void unregister_local_game(struct LocalGame *local, struct Action act);

void add_valid_local_game(struct LocalGame *local, struct Action act);

void clear_actions_local_game(struct LocalGame *local);

/* Doesn't free, just sets to '0' */
void empty_local_game(struct LocalGame *local);
#endif /* MODELS_LOCAL_GAME_H */
