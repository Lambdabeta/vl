#include "local_game.h"

#include <stdlib.h>
#include <stdio.h>

void free_local_game(struct LocalGame *local)
{
    if (local->valid && local->num_valid) free(local->valid);
    if (local->registered) free(local->registered);
}

void generate_local_game(struct LocalGame *local, struct Game game, int team)
{
    local->game = game_local(game,team);

    local->num_valid = list_actions(&local->game, team, &local->valid);
    local->registered = NULL;
    local->num_registered = 0;

    local->remaining = num_actions(&local->game, team);
    local->player = team;
}

static int registered(struct LocalGame *local, struct Action act)
{
    int i;
    for (i = 0; i < local->num_registered; ++i) {
        if (act.source.x == local->registered[i].source.x &&
            act.source.y == local->registered[i].source.y &&
            act.target.x == local->registered[i].target.x &&
            act.target.y == local->registered[i].target.y &&
            act.is_special == local->registered[i].is_special &&
            act.unit == local->registered[i].unit &&
            act.team == local->registered[i].team) 
            return i;
    }
    return -1;
}

static void add_registered(struct LocalGame *local, struct Action act)
{
    int i;
    struct Action *new_registered = malloc(sizeof(struct Action) * 
            (local->num_registered+1));

    for (i = 0; i < local->num_registered; ++i)
        new_registered[i] = local->registered[i];

    new_registered[local->num_registered++] = act;
    local->remaining--;
    if (local->registered) free(local->registered);
    local->registered = new_registered;
}

void register_local_game(struct LocalGame *local, struct Action act)
{
    if (registered(local,act) < 0 &&
        local->remaining > 0) 
        add_registered(local,act);
    else
        printf("Can't register action.\n");
}

void force_register_local_game(struct LocalGame *local, struct Action act)
{
    if (registered(local,act) < 0) 
        add_registered(local,act);
    else
        printf("Can't register action.\n");
}

static void remove_registered(struct LocalGame *local, int index)
{
    int i;
    for (i = index; i < (local->num_registered-1); ++i)
        local->registered[i] = local->registered[i+1];
    local->num_registered--;
    local->remaining++;
}

void unregister_local_game(struct LocalGame *local, struct Action act)
{
    int index = registered(local,act);
    if (index >= 0)
        remove_registered(local,index);
}

void add_valid_local_game(struct LocalGame *local, struct Action act)
{
    int i;
    struct Action *new_valid = malloc(sizeof(struct Action) * 
            (local->num_valid+1));

    for (i = 0; i < local->num_valid; ++i) 
        new_valid[i] = local->valid[i];

    new_valid[local->num_valid++] = act;
    if (local->valid && local->num_valid) free(local->valid); 
    local->valid = new_valid;
}

void clear_actions_local_game(struct LocalGame *local)
{
    if (local->valid) {
        free(local->valid);
        local->valid = NULL;
    }
    if (local->registered) {
        free(local->registered);
        local->registered = NULL;
    }

    local->num_valid = 0;
    local->num_registered = 0;
}

void empty_local_game(struct LocalGame *local)
{
    local->game.tiles = NULL;
    local->game.w = 0;
    local->game.h = 0;
    local->valid = NULL;
    local->num_valid = 0;
    local->registered = NULL;
    local->num_registered = 0;
    local->remaining = 0;
    local->player = 0;
}
