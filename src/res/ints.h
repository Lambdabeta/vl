#ifndef RES_INTS_H
#define RES_INTS_H

extern const int INT_MAX_COLLISION;
extern const int INT_VAMPIRE_VIS;
extern const int INT_LEPRECHAUN_VIS;
extern const int INT_HUMAN_VIS;
extern const int INT_FAIRY_VIS;
extern const int INT_ZOMBIE_VIS;
extern const int INT_MAX_VIS;

extern const int INT_SPLASH_WIDTH;
extern const int INT_SPLASH_HEIGHT;

extern const int INT_WINDOW_WIDTH;
extern const int INT_WINDOW_HEIGHT;

extern const int INT_WAITEVENT_TIMEOUT;

#endif /* RES_INTS_H */
