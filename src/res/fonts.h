#ifndef RES_FONTS_H
#define RES_FONTS_H

#include <SDL2/SDL_ttf.h>

extern TTF_Font *font; /* found in ../screens/init.c */
extern const char *font_file;
extern int font_size;

#endif /* RES_FONTS_H */
