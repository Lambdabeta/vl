#include "sprites.h"

const char *sprite_file[MAX_SPRITE_ID] = {
/*    SPR_BACKGROUND_IMG            */ "res/menus/background.png",
/*    SPR_BUTTON                    */ "res/menus/button.png",
/*    SPR_BUTTON_HOVER              */ "res/menus/button_hover.png",
/*    SPR_BUTTON_PRESSED            */ "res/menus/button_pressed.png",
/*    SPR_CREDITS                   */ "res/credits.png",
/*    SPR_HELP                      */ "res/help.png",

/*    SPR_WAIT_BACKGROUND_IMG       */ "res/wait.png",
/*    SPR_SIDEBAR                   */ "res/sidebar.png",

/*    SPR_TILE_BASE                 */ "res/tiles/base.png",
/*    SPR_TILE_PIT                  */ "res/tiles/disabled.png",
/*    SPR_TILE_FLOOR                */ "res/tiles/enabled.png",
/*    SPR_TILE_PRODUCER             */ "res/tiles/producer.png",
/*    SPR_TILE_UNKNOWN              */ "res/tiles/hidden.png",

/*    SPR_UNIT_VAMPIRE              */ "res/units/vampire.png",
/*    SPR_UNIT_LEPRECHAUN           */ "res/units/leprechaun.png",
/*    SPR_UNIT_HUMAN                */ "res/units/human.png",
/*    SPR_UNIT_FAIRY                */ "res/units/fairy.png",
/*    SPR_UNIT_ZOMBIE               */ "res/units/zombie.png",

/*    SPR_SPAWNED_VAMPIRE           */ "res/units/vampire_spawn.png",
/*    SPR_SPAWNED_LEPRECHAUN        */ "res/units/leprechaun_spawn.png",
/*    SPR_SPAWNED_HUMAN             */ "res/units/human_spawn.png",
/*    SPR_SPAWNED_FAIRY             */ "res/units/fairy_spawn.png",
/*    SPR_SPAWNED_ZOMBIE            */ "res/units/zombie_spawn.png",

/*    SPR_UNSPAWN_OPTION            */ "res/units/x.png",

/*    SPR_ENEMY_UNIT_VAMPIRE        */ "res/units/vampire_inactive.png",
/*    SPR_ENEMY_UNIT_LEPRECHAUN     */ "res/units/leprechaun_inactive.png",
/*    SPR_ENEMY_UNIT_HUMAN          */ "res/units/human_inactive.png",
/*    SPR_ENEMY_UNIT_FAIRY          */ "res/units/fairy_inactive.png",
/*    SPR_ENEMY_UNIT_ZOMBIE         */ "res/units/zombie_inactive.png",

/*    SPR_SPAWN_VAMPIRE             */ "res/vampire_choice.png",
/*    SPR_SPAWN_LEPRECHAUN          */ "res/leprechaun_choice.png",
/*    SPR_SPAWN_HUMAN               */ "res/human_choice.png",
/*    SPR_SPAWN_FAIRY               */ "res/fairy_choice.png",
/*    SPR_SPAWN_ZOMBIE              */ "res/zombie_choice.png",
/*    SPR_SPAWN_NONE                */ "res/cancel_choice.png",

/*    SPR_ARROW_ACT                 */ "res/arrows/east_act.png",

/*    SPR_ARROW_MOVE                */ "res/arrows/east.png",

/*    SPR_SELECT_ACT                */ "res/selection_act.png",
/*    SPR_SELECT_MOVE               */ "res/selection_move.png",

/*    MAX_SPRITE_ID                 */ NULL };
