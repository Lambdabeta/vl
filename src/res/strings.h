#ifndef RES_STRINGS_H
#define RES_STRINGS_H

extern const char *STR_ERROR_TITLE;
extern const char *STR_SDL_INIT_ERROR;
extern const char *STR_IMG_INIT_ERROR;
extern const char *STR_TTF_INIT_ERROR;
extern const char *STR_MIX_INIT_ERROR;
extern const char *STR_WIN_INIT_ERROR;
extern const char *STR_REN_INIT_ERROR;

extern const char *STR_WAIT_ERROR;

extern const char *STR_IMG_LOAD_ERROR; /* %s = file */
extern const char *STR_IMG_CONVERT_ERROR; /* %s = file */
extern const char *STR_TTF_LOAD_ERROR; /* %s = file */

extern const char *STR_SPLASH_TITLE;
extern const char *STR_WINDOW_TITLE;

extern const char *STR_LOAD_MESSAGE;

extern const char *STR_HOST;
extern const char *STR_JOIN;
extern const char *STR_CREDITS;
extern const char *STR_HELP;
extern const char *STR_BACK;
extern const char *STR_OKAY;
extern const char *STR_PREVIOUS;
extern const char *STR_NEXT;

extern const char *STR_HOST_TITLE;
extern const char *STR_JOIN_TITLE;

#endif /* RES_STRINGS_H */
