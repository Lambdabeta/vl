#ifndef RES_GLOBALS_H
#define RES_GLOBALS_H

#include <SDL2/SDL.h>

extern SDL_Window *window;
extern SDL_Renderer *renderer;

#endif /* RES_GLOBALS_H */
