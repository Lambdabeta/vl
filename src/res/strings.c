 const char *STR_ERROR_TITLE = "ERROR";
 const char *STR_SDL_INIT_ERROR = "SDL_INIT ERROR";
 const char *STR_IMG_INIT_ERROR = "IMG_INIT ERROR";
 const char *STR_TTF_INIT_ERROR = "TTF_INIT ERROR";
 const char *STR_MIX_INIT_ERROR = "MIX_INIT ERROR";
 const char *STR_WIN_INIT_ERROR = "WIN_INIT ERROR";
 const char *STR_REN_INIT_ERROR = "REN_INIT ERROR";

 const char *STR_WAIT_ERROR = "WAIT ERROR";

 const char *STR_IMG_LOAD_ERROR = "CANT LOAD %s ..."; /* %s = file */
 const char *STR_IMG_CONVERT_ERROR = "CANT CONVERT %s ..."; /* %s = file */
 const char *STR_TTF_LOAD_ERROR = "CANT OPEN %s ..."; /* %s = file */

 const char *STR_SPLASH_TITLE = "Loading VL";
 const char *STR_WINDOW_TITLE = "VL";

 const char *STR_LOAD_MESSAGE = "Loading...";

 const char *STR_HOST = "HOST";
 const char *STR_JOIN = "JOIN";
 const char *STR_CREDITS = "CREDITS";
 const char *STR_HELP = "HELP";
 const char *STR_BACK = "BACK";
 const char *STR_OKAY = "OKAY";
 const char *STR_PREVIOUS = "<";
 const char *STR_NEXT = ">";

 const char *STR_HOST_TITLE = "HOSTING";
 const char *STR_JOIN_TITLE = "JOINING";
