#ifndef RES_COLOURS_H
#define RES_COLOURS_H

extern SDL_Color COL_BUTTON_TEXT;
extern SDL_Color COL_STRING_INPUT_FG;
extern SDL_Color COL_STRING_INPUT_BG;
extern SDL_Color COL_MENU_TITLE;

#endif /* RES_COLOURS_H */
