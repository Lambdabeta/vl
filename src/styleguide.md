# LAB Styleguide for C

Designed for ease of navigation and understanding on a 80x24+ screen.

## Modules 

Each module must do one specific task and may rely on other modules to do them.
The goal is to have highly cohesive modules with minimal coupling. This is
achieved via two rules:

 1. Modules may only export one new datatype. (coupling)
 2. Modules' source code must not exceed 200 lines of code. (cohesion)

## Functions

All functions must be in a canonical format. These are the canonical formats:

### Failable Call

Used for calling functions which may fail, responding to the failure, and
returning a value.

```
    {
        0-3 lines of setup
        
        1-10 lines of compound if statement
            1-5 lines of fail response
            1 line of fail return
        0-1 line of else
            0-5 lines of good response
        1 line of good return
    }
```
        

