#include "list.h"

#include <SDL2/SDL.h>

#include <stdio.h>

#include "../tools/button.h"
#include "../tools/gamefile.h"
#include "../tools/centered.h"
#include "../tools/debug.h"
#include "../views/game.h"
#include "../res/fonts.h"
#include "../res/colours.h"
#include "../res/strings.h"
#include "../res/rects.h"
#include "../res/globals.h"

struct HostMenuData {
    struct Button prev,next,okay,back;
    int curmap;
    int num_maps;
    char **map_names;
    struct Game game;
};

static void load_maps(struct HostMenuData *me)
{
    int i;
    FILE *file = fopen("map/list","r");

    if (!file) {
        ERROR("Can't open map/list!");
        return;
    }

    fscanf(file,"%d\n",&me->num_maps);

    me->map_names = malloc(sizeof(char*) * me->num_maps);
    for (i = 0; i < me->num_maps; ++i) {
        me->map_names[i] = malloc(sizeof(char) * 256);

        fgets(me->map_names[i],256,file);

        me->map_names[i][strlen(me->map_names[i]) - 1] = '\0';
    }

    me->curmap = 0;

    me->game = gamefile_load(me->map_names[me->curmap]);
    fclose(file);
}

static void host_menu_load(struct ScreenFunc *this)
{
    struct HostMenuData *me = malloc(sizeof(struct HostMenuData));

    me->prev = button_new(STR_PREVIOUS,RECT_BUTTON_PREVIOUS);
    me->next = button_new(STR_NEXT,RECT_BUTTON_NEXT);
    me->back = button_new(STR_BACK,RECT_BUTTON_BACK);
    me->okay = button_new(STR_OKAY,RECT_BUTTON_OKAY);

    load_maps(me);

    this->data = me;
}

static void host_menu_close(struct ScreenFunc *this)
{
    int i;
    struct HostMenuData *hmd = this->data;
    button_free(hmd->prev);
    button_free(hmd->next);
    button_free(hmd->back);
    button_free(hmd->okay);

    for (i = 0; i < hmd->num_maps; ++i)
        free(hmd->map_names[i]);
    free(hmd->map_names);

    if (this->run == host_wait) {
        this->data = malloc(sizeof(struct Game));
        memcpy(this->data,&hmd->game,sizeof(hmd->game));
    } else free_game(hmd->game);

    free(hmd);
}

static void host_menu_draw(struct ScreenFunc *this)
{
    struct HostMenuData *hmd = this->data;
    SDL_RenderClear(renderer);

    gameview_draw(hmd->game, 0, RECT_MAP_AREA);

    button_draw(hmd->prev);
    button_draw(hmd->next);
    button_draw(hmd->back);
    button_draw(hmd->okay);

    SDL_RenderPresent(renderer);
}

static void host_menu_dec_map(struct HostMenuData *this)
{
    if (this->game.tiles) free_game(this->game);
    this->curmap--; 
    if (this->curmap < 0) this->curmap = this->num_maps-1;
    this->game = gamefile_load(this->map_names[this->curmap]);
}

static void host_menu_inc_map(struct HostMenuData *this)
{
    if (this->game.tiles) free_game(this->game);
    this->curmap++;
    if (this->curmap >= this->num_maps) this->curmap = 0;
    this->game = gamefile_load(this->map_names[this->curmap]);
}

static int host_menu_handle(SDL_Event e, struct ScreenFunc *this)
{
    struct HostMenuData *hmd = this->data;
    if (e.type == SDL_QUIT) {
        this->run = NULL;
        return 1;
    }
    if (button_process(&hmd->back,&e)) { this->run = main_menu; return 1; }
    if (button_process(&hmd->okay,&e)) { this->run = host_wait; return 1; }
    if (button_process(&hmd->prev,&e)) { host_menu_dec_map(hmd); }
    if (button_process(&hmd->next,&e)) { host_menu_inc_map(hmd); }
    return 0;
}

void host_menu(struct ScreenFunc *this)
{
    SDL_Event e;
    int quit = 0;

    host_menu_load(this);
    host_menu_draw(this);
    while (!quit) {
        SDL_WaitEvent(&e);
        quit = host_menu_handle(e,this);
        host_menu_draw(this);
    }
    host_menu_close(this);
}
