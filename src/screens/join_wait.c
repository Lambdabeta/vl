#include "list.h"

#include <SDL2/SDL.h>

#include "../res/rects.h"
#include "../res/strings.h"
#include "../res/globals.h"
#include "../res/sprites.h"
#include "../res/ints.h"
#include "../tools/button.h"
#include "../server/tcp.h"


struct JoinWaitData {
    struct Button back;
    struct VL_API *client;
};

static void join_wait_load(struct ScreenFunc *this)
{
    const char *ip = strtok(this->data,":");
    const char *port = strtok(NULL,":");
    struct JoinWaitData *me = malloc(sizeof(struct JoinWaitData));

    me->back = button_new(STR_BACK,RECT_BUTTON_BACK);
    me->client = create_tcp_client(ip,atoi(port));

    free(this->data);
    this->data = me;
}

static void join_wait_close(struct ScreenFunc *this)
{
    struct JoinWaitData *me = this->data;

    button_free(me->back);

    if (this->run == game_screen)
        this->data = me->client;
    else
        free_tcp(me->client);
    free(me);
}

static void join_wait_draw(struct ScreenFunc *this)
{
    struct JoinWaitData *me = this->data;
    SDL_RenderClear(renderer);

    SDL_RenderCopy(renderer,sprites[SPR_WAIT_BACKGROUND_IMG],NULL,NULL);

    button_draw(me->back);

    SDL_RenderPresent(renderer);
}

static int join_wait_update(struct ScreenFunc *this)
{
    struct JoinWaitData *me = this->data;

    if (update_api(me->client) != -1) { this->run = game_screen; return 1; }

    return 0;
}

static int join_wait_handle(SDL_Event e, struct ScreenFunc *this)
{
    struct JoinWaitData *me = this->data;

    if (e.type == SDL_QUIT) { this->run = NULL; return 1; }
    if (button_process(&me->back,&e)) { this->run = main_menu; return 1; }

    return join_wait_update(this);
}

void join_wait(struct ScreenFunc *this)
{
    SDL_Event e;
    int quit = 0;

    join_wait_load(this);
    join_wait_draw(this);
    while (!quit) {
        if (SDL_WaitEventTimeout(&e,INT_WAITEVENT_TIMEOUT))
            quit |= join_wait_handle(e,this);
        else
            quit |= join_wait_update(this);
        join_wait_draw(this);
    }
    join_wait_close(this);
}
