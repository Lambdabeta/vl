#include "list.h"

#include <SDL2/SDL.h>

#include "../res/globals.h"
#include "../res/ints.h"
#include "../res/rects.h"
#include "../res/strings.h"
#include "../res/sprites.h"
#include "../tools/button.h"
#include "../tools/centered.h"
#include "../server/tcp.h"

struct HostWaitData {
    struct Button back;
    struct VL_API *server;
};

static void host_wait_load(struct ScreenFunc *this)
{
    struct HostWaitData *me = malloc(sizeof(struct HostWaitData));

    me->back = button_new(STR_BACK,RECT_BUTTON_BACK);
    me->server = create_tcp_server(*(struct Game *)this->data);

    printf("Port: %d\n",get_tcp_port(me->server));

    this->data = me;
}

static void host_wait_close(struct ScreenFunc *this)
{
    struct HostWaitData *me = this->data;

    button_free(me->back);

    if (this->run == game_screen)
        this->data = me->server;
    else
        free_tcp(me->server);
    free(me);
}

static void host_wait_draw(struct ScreenFunc *this)
{
    struct HostWaitData *me = this->data;
    SDL_RenderClear(renderer);

    SDL_RenderCopy(renderer,sprites[SPR_WAIT_BACKGROUND_IMG],NULL,NULL);

    button_draw(me->back);

    SDL_RenderPresent(renderer);
}

static int host_wait_update(struct ScreenFunc *this)
{
    struct HostWaitData *me = this->data;

    if (update_api(me->server) != -1) { this->run = game_screen; return 1; }

    return 0;
}

static int host_wait_handle(SDL_Event e, struct ScreenFunc *this)
{
    struct HostWaitData *me = this->data;

    if (e.type == SDL_QUIT) { this->run = NULL; return 1; }
    if (button_process(&me->back,&e)) { this->run = main_menu; return 1; }

    return host_wait_update(this);
}

void host_wait(struct ScreenFunc *this)
{
    SDL_Event e;
    int quit = 0;

    host_wait_load(this);
    host_wait_draw(this);
    while (!quit) {
        if (SDL_WaitEventTimeout(&e,INT_WAITEVENT_TIMEOUT))
            quit |= host_wait_handle(e,this);
        else
            quit |= host_wait_update(this);
        host_wait_draw(this);
    }
    host_wait_close(this);
}
