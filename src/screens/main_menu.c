#include "list.h"

#include <SDL2/SDL.h>

#include "../tools/button.h"
#include "../res/strings.h"
#include "../res/rects.h"
#include "../res/globals.h"
#include "../res/sprites.h"

struct MainMenuData {
    struct Button host,join,credits,help;
};

static void main_menu_load(struct ScreenFunc *this)
{
    struct MainMenuData *me = malloc(sizeof(struct MainMenuData));

    me->host = button_new(STR_HOST,RECT_BUTTON_HOST);
    me->join = button_new(STR_JOIN,RECT_BUTTON_JOIN);
    me->credits = button_new(STR_CREDITS,RECT_BUTTON_CREDITS);
    me->help = button_new(STR_HELP,RECT_BUTTON_HELP);

    this->data = me;
}

static void main_menu_close(struct ScreenFunc *this)
{
    struct MainMenuData *mmd = this->data;
    button_free(mmd->host);
    button_free(mmd->join);
    button_free(mmd->credits);
    button_free(mmd->help);
    free(this->data);
}

static void main_menu_draw(struct ScreenFunc *this)
{
    struct MainMenuData *mmd = this->data;
    SDL_RenderClear(renderer);

    SDL_RenderCopy(renderer,sprites[SPR_BACKGROUND_IMG],NULL,NULL);

    button_draw(mmd->host);
    button_draw(mmd->join);
    button_draw(mmd->credits);
    button_draw(mmd->help);

    SDL_RenderPresent(renderer);
} 

static int main_menu_handle(SDL_Event e, struct ScreenFunc *this)
{
    struct MainMenuData *mmd = this->data;
    switch (e.type) {
        case SDL_QUIT: this->run = NULL; return 1;
        default:
            if (button_process(&mmd->host,&e)) { this->run = host_menu; return 1; }
            if (button_process(&mmd->join,&e)) { this->run = join_menu; return 1; }
            if (button_process(&mmd->credits,&e)) { this->run = credits; return 1; }
            if (button_process(&mmd->help,&e)) { this->run = help; return 1; }
    }
    return 0;
}

void main_menu(struct ScreenFunc *this)
{
    SDL_Event e;
    int quit = 0;

    main_menu_load(this);
    main_menu_draw(this);
    while (!quit) {
        SDL_WaitEvent(&e);
        quit = main_menu_handle(e,this);
        main_menu_draw(this);
    }
    main_menu_close(this);
}
