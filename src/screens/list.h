#ifndef SCREENS_LIST_H
#define SCREENS_LIST_H

struct ScreenFunc {
    void (*run)(struct ScreenFunc *);
    void *data;
};

struct ScreenFunc first_screen(void);

/* These functions are each in their own source files. */
void main_menu(struct ScreenFunc *);
void host_menu(struct ScreenFunc *);
void join_menu(struct ScreenFunc *);
void credits(struct ScreenFunc *);
void help(struct ScreenFunc *);
void join_wait(struct ScreenFunc *); /* Takes an ip:port string in data. */
void host_wait(struct ScreenFunc *); /* Takes a game in data. */
void game_screen(struct ScreenFunc *); /* Takes a VLTCP in data. */

#endif /* SCREENS_LIST_H */
