#include "list.h"

#include "../views/game.h"
#include "../views/action.h"
#include "../server/tcp.h"
#include "../res/rects.h"
#include "../res/ints.h"
#include "../res/globals.h"
#include "../res/sprites.h"
#include "../tools/within.h"

#include <SDL2/SDL.h>

struct GameScreenData {
    struct VL_API *tcp;
    int selected_x; /* -1 if nothing selected. */
    int selected_y; /* -1 if nothing selected. */
    enum UnitType spawn; /* Unknown if still deciding. */
};

static void game_screen_load(struct ScreenFunc *this)
{
    struct GameScreenData *me = malloc(sizeof(struct GameScreenData));
    me->tcp = this->data;
    me->selected_x = -1;
    me->selected_y = -1;
    me->spawn = UNIT_TYPE_UNKNOWN;

    this->data = me;
}

static void game_screen_close(struct ScreenFunc *this)
{
    struct GameScreenData *me = this->data;
    free_tcp(me->tcp);
    free(me);
    this->run = NULL;
}

static void game_screen_draw_remove_spawn(struct GameScreenData *this)
{
    int i;
    struct LocalGame *game = game_api(this->tcp);

    for (i = 0; i < game->num_registered; ++i) {
        if (game->registered[i].source.x == game->registered[i].target.x &&
            game->registered[i].source.y == game->registered[i].target.y) {
            render_unspawn_action(game->registered[i],RECT_BOARD_AREA,
                    game->game.w, game->game.h);
            return;
        }
    }

    /* No unspawns available, deselect. */
    this->selected_x = -1;
    this->selected_y = -1;
}

static void game_screen_draw_spawn(struct GameScreenData *this)
{
    if (this->spawn == UNIT_TYPE_UNKNOWN) {
        render_spawn_selection(RECT_BOARD_AREA);
    } else if (this->spawn == UNIT_TYPE_NONE) {
        game_screen_draw_remove_spawn(this);
    } else {
        int i;
        struct LocalGame *game = game_api(this->tcp);
        for (i = 0; i < game->num_valid; ++i)
            if (action_is_spawn(game->valid[i]) &&
                game->valid[i].unit == this->spawn)
                render_action_option(game->valid[i],RECT_BOARD_AREA,
                        game->game.w, game->game.h);
    }
}

static void game_screen_render_registered(struct GameScreenData *this)
{
    int i;
    struct LocalGame *game = game_api(this->tcp);
    for (i = 0; i < game->num_registered; ++i) 
        render_action(game->registered[i], RECT_BOARD_AREA, 
                game->game.w, game->game.h);
}

static void game_screen_draw_actions(struct GameScreenData *this)
{
    struct LocalGame *game = game_api(this->tcp);

    game_screen_render_registered(this);

    if (this->selected_x >= 0 && this->selected_y >= 0) {
        struct Tile selected = game->game.tiles[this->selected_y][this->selected_x];
        if (selected.type == TILE_TYPE_BASE && selected.team == game->player)
            game_screen_draw_spawn(this);
        else {
            int i;
            for (i = 0; i < game->num_valid; ++i)
                if (game->valid[i].source.x == this->selected_x &&
                    game->valid[i].source.y == this->selected_y)
                    render_action_option(game->valid[i],RECT_BOARD_AREA,
                            game->game.w, game->game.h);
        }
    }
}

static void game_screen_draw(struct ScreenFunc *this)
{
    struct GameScreenData *me = this->data;
    struct LocalGame *game = game_api(me->tcp);
    SDL_RenderClear(renderer);

    gameview_draw(game->game,game->player,RECT_BOARD_AREA);
    game_screen_draw_actions(me);
    SDL_RenderCopy(renderer,sprites[SPR_SIDEBAR],NULL,&RECT_COMMIT_AREA);
    if (game->remaining < 0)
        SDL_RenderCopy(renderer,sprites[SPR_WAIT_BACKGROUND_IMG],NULL,&RECT_COMMIT_AREA);

    SDL_RenderPresent(renderer);
}

static int game_screen_update(struct ScreenFunc *this)
{
    struct GameScreenData *me = this->data;
    return update_api(me->tcp);
}

static void game_screen_execute_spawn(struct GameScreenData *me, int sx, int sy)
{
    int i;
    struct LocalGame *game = game_api(me->tcp);

    for (i = 0; i < game->num_valid; ++i) {
        if (game->valid[i].target.x == sx &&
            game->valid[i].target.y == sy &&
            action_is_spawn(game->valid[i])) {
            register_api(me->tcp,game->valid[i]);
            break;
        }
    }
    me->selected_x = me->selected_y = -1;
}

static void game_screen_commit(struct GameScreenData *me)
{
    commit_api(me->tcp);
}

static void game_screen_execute_move(struct GameScreenData *me, int sx, int sy)
{
    int i;
    struct LocalGame *game = game_api(me->tcp);

    for (i = 0; i < game->num_valid; ++i) {
        struct Action act = game->valid[i];
        if (act.source.x == me->selected_x &&
            act.source.y == me->selected_y &&
            act.target.x == sx &&
            act.target.y == sy &&
            !act.is_special && act.team == game->player) {
            register_api(me->tcp,act);
            break;
        }
    }
    me->selected_x = me->selected_y = -1;
}

static void game_screen_execute_action(struct GameScreenData *me, int sx, int sy)
{
    int i;
    struct LocalGame *game = game_api(me->tcp);

    for (i = 0; i < game->num_valid; ++i) {
        struct Action act = game->valid[i];
        if (act.source.x == me->selected_x &&
            act.source.y == me->selected_y &&
            act.target.x == sx &&
            act.target.y == sy &&
            act.is_special && act.team == game->player) {
            register_api(me->tcp,act);
            break;
        }
    }
    me->selected_x = me->selected_y = -1;
}

static void game_screen_select(struct GameScreenData *me, int sx, int sy)
{
    int i;
    struct LocalGame *game = game_api(me->tcp);

    for (i = 0; i < game->num_registered; ++i) {
        struct Action act = game->registered[i];
        if (act.source.x == sx && act.source.y == sy)
            unregister_api(me->tcp,act);
    }
    me->selected_x = sx;
    me->selected_y = sy;
}

static void game_screen_execute_at(struct GameScreenData *this, struct LocalGame *game, int x, int y)
{
    struct Tile selected = game->game.tiles[this->selected_y][this->selected_x];
    if (selected.type == TILE_TYPE_BASE && selected.team == game->player) {
        if (this->spawn == UNIT_TYPE_UNKNOWN)
            this->spawn = get_spawn_selection(x,y,RECT_BOARD_AREA);
        else
            game_screen_execute_spawn(this,
                    get_tile_x(x,RECT_BOARD_AREA,game->game.w),
                    get_tile_y(y,RECT_BOARD_AREA,game->game.h));
    } else {
        game_screen_execute_move(this,
                    get_tile_x(x,RECT_BOARD_AREA,game->game.w),
                    get_tile_y(y,RECT_BOARD_AREA,game->game.h));
    }
}

static int game_screen_handle(SDL_Event e, struct ScreenFunc *this)
{
    struct GameScreenData *me = this->data;
    struct LocalGame *game = game_api(me->tcp);
    int sx = get_tile_x(e.button.x,RECT_BOARD_AREA,game->game.w);
    int sy = get_tile_y(e.button.y,RECT_BOARD_AREA,game->game.h);

    if (e.type == SDL_QUIT) return 1;
    if (e.type == SDL_MOUSEBUTTONDOWN && e.button.button == SDL_BUTTON_LEFT) {
        if (!within(e.button.x,e.button.y,RECT_BOARD_AREA)) 
            game_screen_commit(me);
        if (me->selected_x >= 0 && me->selected_y >= 0) {
            game_screen_execute_at(me,game,e.button.x,e.button.y);
        } else if (sx >= 0 && sy >= 0 &&
                   sx < game->game.w && sy < game->game.h &&
                   game->game.tiles[sy][sx].team == game->player) {
            game_screen_select(me,sx,sy);
        } else {
            me->selected_x = me->selected_y = -1;
        }
    }

    if (e.type == SDL_MOUSEBUTTONDOWN && e.button.button == SDL_BUTTON_RIGHT &&
        me->selected_x >= 0 && me->selected_y >= 0) {
        game_screen_execute_action(me,sx,sy);
    }

    return 0;
}
    
void game_screen(struct ScreenFunc *this)
{
    SDL_Event e;
    int quit = 0;

    game_screen_load(this);
    game_screen_draw(this);
    while (!quit) {
        if (SDL_WaitEventTimeout(&e,INT_WAITEVENT_TIMEOUT))
            quit |= game_screen_handle(e,this);
        else
            quit |= game_screen_update(this);
        game_screen_draw(this);
    }
    game_screen_close(this);
}
