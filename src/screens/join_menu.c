#include "list.h"

#include <SDL2/SDL.h>

#include "../tools/button.h"
#include "../tools/string_input.h"
#include "../tools/centered.h"
#include "../tools/font.h"
#include "../res/fonts.h"
#include "../res/colours.h"
#include "../res/strings.h"
#include "../res/rects.h"
#include "../res/globals.h"
#include "../res/sprites.h"

struct JoinMenuData {
    struct Button back,okay;
    SDL_Texture *title; int w,h;
    struct StringInput *ipp;
};

static void join_menu_load(struct ScreenFunc *this)
{
    struct JoinMenuData *me = malloc(sizeof(struct JoinMenuData));

    me->back = button_new(STR_BACK,RECT_BUTTON_BACK);
    me->okay = button_new(STR_OKAY,RECT_BUTTON_OKAY);
    me->ipp = string_input_new(RECT_STR_INPUT);
    me->title = render_text(font,STR_JOIN_TITLE,COL_MENU_TITLE,&me->w,&me->h);

    this->data = me;
}

static void join_menu_close(struct ScreenFunc *this)
{
    struct JoinMenuData *jmd = this->data;
    button_free(jmd->back);
    button_free(jmd->okay);
    SDL_DestroyTexture(jmd->title);

    if (this->run == join_wait) {
        this->data = malloc(sizeof(char) * strlen(string_input_string(jmd->ipp)));
        strcpy(this->data,string_input_string(jmd->ipp));
    }

    string_input_free(jmd->ipp);
    free(jmd);
}

static void join_menu_draw(struct ScreenFunc *this)
{
    struct JoinMenuData *jmd = this->data;
    SDL_RenderClear(renderer);

    SDL_RenderCopy(renderer,sprites[SPR_BACKGROUND_IMG],NULL,NULL);

    button_draw(jmd->back);
    button_draw(jmd->okay);

    string_input_draw(jmd->ipp);
    render_centered(jmd->title,jmd->w,jmd->h,RECT_MENU_TITLE);

    SDL_RenderPresent(renderer);
}

static int join_menu_handle(SDL_Event e, struct ScreenFunc *this)
{
    struct JoinMenuData *jmd = this->data;
    switch (e.type) {
        case SDL_QUIT: this->run = NULL; return 1;
        default:
            if (button_process(&jmd->back,&e)) { this->run = main_menu; return 1; }
            if (button_process(&jmd->okay,&e)) { this->run = join_wait; return 1; }
            string_input_process(jmd->ipp,&e);
    }
    return 0;
}

void join_menu(struct ScreenFunc *this)
{
    SDL_Event e;
    int quit = 0;
    
    join_menu_load(this);
    join_menu_draw(this);
    while (!quit) {
        SDL_WaitEvent(&e);
        quit = join_menu_handle(e,this);
        join_menu_draw(this);
    }
    join_menu_close(this);
}
