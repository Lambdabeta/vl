#include "list.h"

#include <SDL2/SDL.h>

#include "../tools/button.h"
#include "../res/rects.h"
#include "../res/strings.h"
#include "../res/globals.h"
#include "../res/sprites.h"

void help(struct ScreenFunc *this)
{
    SDL_Event e;
    struct Button back = button_new(STR_BACK, RECT_BUTTON_RETURN);

    do {
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer,sprites[SPR_HELP],NULL,NULL);
        button_draw(back);
        SDL_RenderPresent(renderer);

        SDL_WaitEvent(&e);
    } while (e.type != SDL_QUIT && !button_process(&back,&e));

    button_free(back);
    this->run = main_menu;
}



