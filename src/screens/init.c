#include "init.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "../tools/debug.h"
#include "../res/ints.h"
#include "../res/strings.h"
#include "../res/sprites.h"
#include "../res/fonts.h"
#include "../res/rects.h"

SDL_Texture *sprites[MAX_SPRITE_ID];
TTF_Font *font;

static SDL_Renderer *ren;
static SDL_Window *wnd;

static int init_init_screen(void)
{
    wnd = SDL_CreateWindow(
            STR_SPLASH_TITLE,
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            INT_SPLASH_WIDTH,
            INT_SPLASH_HEIGHT,
            SDL_WINDOW_SHOWN);

    if (!wnd) {
        ERROR(STR_WIN_INIT_ERROR);
        return 0;
    }

    ren = SDL_CreateRenderer(wnd,-1,SDL_RENDERER_ACCELERATED);
    if (!ren) {
        ERROR(STR_REN_INIT_ERROR);
        return 0;
    }

    font = TTF_OpenFont(font_file,font_size);
    if (!font) {
        ERROR(STR_TTF_LOAD_ERROR,font_file);
        return 0;
    }

    return 1;
}

static void init_screen_init_draw(void)
{
    SDL_Color col = {0xFF, 0xFF, 0xFF, 0xFF};
    SDL_Surface *tmp_sur;
    SDL_Texture *fnt_tex;

    SDL_SetRenderDrawColor(ren, 0x00, 0x00, 0x00, 0xFF);
    SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);
    SDL_RenderClear(ren);

    tmp_sur = TTF_RenderText_Solid(font,STR_LOAD_MESSAGE,col);
    fnt_tex = SDL_CreateTextureFromSurface(ren,tmp_sur);
    SDL_FreeSurface(tmp_sur);

    SDL_RenderCopy(ren,fnt_tex,NULL,&RECT_SPLASH_MESSAGE);
    SDL_DestroyTexture(fnt_tex);

    SDL_SetRenderDrawColor(ren, 0xFF, 0x00, 0x00, 0xFF);
    SDL_RenderDrawRect(ren,&RECT_SPLASH_PROGRESS);

    SDL_RenderPresent(ren);
}

static void clean_to(int i)
{
    int x;
    for (x = 0; x < i; ++x) 
        SDL_FreeSurface((SDL_Surface*)sprites[x]);
}

static void update_render(int i)
{
    SDL_Rect r;
    r.x = RECT_SPLASH_PROGRESS.x;
    r.y = RECT_SPLASH_PROGRESS.y;
    r.h = RECT_SPLASH_PROGRESS.h;
    r.w = (RECT_SPLASH_PROGRESS.w * i) / MAX_SPRITE_ID;
    SDL_RenderFillRect(ren,&r);

    SDL_RenderPresent(ren);
}

void init_screen_run(void)
{
    int i;
    if (!init_init_screen()) return; 
    
    for (i = 0; i < MAX_SPRITE_ID; ++i) {
        SDL_Surface *tmp = IMG_Load(sprite_file[i]);
        if (!tmp) {
            ERROR(STR_IMG_LOAD_ERROR,sprite_file[i]);
            clean_to(i);
            return;
        }

        sprites[i] = (SDL_Texture*)tmp;

        update_render(i);
    }

    SDL_DestroyRenderer(ren);
    SDL_DestroyWindow(wnd);
}
