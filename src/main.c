#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include <stdlib.h>

#include "tools/boilerplate.h"
#include "tools/debug.h"
#include "screens/init.h"
#include "screens/list.h"

int main(int argc, char *argv[])
{
    struct ScreenFunc func;
    if (boil_init()) {
        ERROR("COULDNT INIT");
        return EXIT_FAILURE;
    }

    /* Init screen uses a splash window. */
    init_screen_run();

    if (boil_init_window()) {
        ERROR("COULDNT CREATE WINDOW");
        return EXIT_FAILURE;
    }

    func = first_screen();
    while (func.run) {
        func.run(&func);
    }   

    boil_close();
    return EXIT_SUCCESS;
}
    
